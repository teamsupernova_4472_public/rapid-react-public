//
// Inputs
//
Inputs
{
	Mat source0;
}

//
// Variables
//
Outputs
{
	Mat hsvThresholdOutput;
	Mat cvErodeOutput;
	Mat cvDilateOutput;
	ContoursReport findContoursOutput;
}

//
// Steps
//

Step HSV_Threshold0
{
    Mat hsvThresholdInput = source0;
    List hsvThresholdHue = [0.0, 114.84848484848483];
    List hsvThresholdSaturation = [38.98381294964029, 255.0];
    List hsvThresholdValue = [38.98381294964029, 255.0];

    hsvThreshold(hsvThresholdInput, hsvThresholdHue, hsvThresholdSaturation, hsvThresholdValue, hsvThresholdOutput);
}

Step CV_erode0
{
    Mat cvErodeSrc = hsvThresholdOutput;
    Mat cvErodeKernel;
    Point cvErodeAnchor = (-1, -1);
    Double cvErodeIterations = 1;
    BorderType cvErodeBordertype = BORDER_CONSTANT;
    Scalar cvErodeBordervalue = (-1);

    cvErode(cvErodeSrc, cvErodeKernel, cvErodeAnchor, cvErodeIterations, cvErodeBordertype, cvErodeBordervalue, cvErodeOutput);
}

Step CV_dilate0
{
    Mat cvDilateSrc = cvErodeOutput;
    Mat cvDilateKernel;
    Point cvDilateAnchor = (-1, -1);
    Double cvDilateIterations = 10.0;
    BorderType cvDilateBordertype = BORDER_CONSTANT;
    Scalar cvDilateBordervalue = (-1);

    cvDilate(cvDilateSrc, cvDilateKernel, cvDilateAnchor, cvDilateIterations, cvDilateBordertype, cvDilateBordervalue, cvDilateOutput);
}

Step Find_Contours0
{
    Mat findContoursInput = cvDilateOutput;
    Boolean findContoursExternalOnly = false;

    findContours(findContoursInput, findContoursExternalOnly, findContoursOutput);
}




