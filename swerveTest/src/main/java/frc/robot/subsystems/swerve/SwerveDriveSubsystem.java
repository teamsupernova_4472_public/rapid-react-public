// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems.swerve;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.kinematics.ChassisSpeeds;
import edu.wpi.first.math.kinematics.SwerveDriveKinematics;
import edu.wpi.first.math.kinematics.SwerveDriveOdometry;
import edu.wpi.first.math.kinematics.SwerveModuleState;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.subsystems.swerve.configurations.ISwerveModuleConfiguration;
import frc.robot.subsystems.swerve.configurations.SwerveAndSteerModuleConfig;
import frc.robot.util.PIDConstants;
import static frc.robot.Constants.*;

import com.kauailabs.navx.frc.AHRS;

public class SwerveDriveSubsystem extends SubsystemBase {
  private final SwerveModule[] mModules;
  private final SwerveDriveKinematics mKinematics;
  private final Gyro mGyro;
  private final SwerveDriveOdometry mOdometry;

  public SwerveDriveSubsystem() {
    ISwerveModuleConfiguration frontLeftConfiguration = new SwerveAndSteerModuleConfig.Builder()
      .withDriveCanId(FRONT_LEFT_DRIVE_CAN_ID)
      .withTurnPwmPort(FRONT_LEFT_TURN_PWM_PORT)
      .withTurnAbsEncAnalogPortAndOffset(FRONT_LEFT_TURN_ABS_ENC_PORT, FRONT_LEFT_TURN_ABS_ENC_OFFSET)
      .withRelEncDioPorts(FRONT_LEFT_TURN_REL_ENC_PORT_A, FRONT_LEFT_TURN_REL_ENC_PORT_B)
      .withTurnPIDConstants(new PIDConstants(TURN_P, TURN_I, TURN_D))
      .withDrivePIDConstants(new PIDConstants(DRIVE_P, DRIVE_I, DRIVE_D, DRIVE_F))
      .build();
    ISwerveModuleConfiguration frontRightConfiguration = new SwerveAndSteerModuleConfig.Builder()
      .withDriveCanId(FRONT_RIGHT_DRIVE_CAN_ID)
      .withTurnPwmPort(FRONT_RIGHT_TURN_PWM_PORT)
      .withTurnAbsEncAnalogPortAndOffset(FRONT_RIGHT_TURN_ABS_ENC_PORT, FRONT_RIGHT_TURN_ABS_ENC_OFFSET)
      .withRelEncDioPorts(FRONT_RIGHT_TURN_REL_ENC_PORT_A, FRONT_RIGHT_TURN_REL_ENC_PORT_B)
      .withTurnPIDConstants(new PIDConstants(TURN_P, TURN_I, TURN_D))
      .withDrivePIDConstants(new PIDConstants(DRIVE_P, DRIVE_I, DRIVE_D, DRIVE_F))
      .build();
    ISwerveModuleConfiguration rearLeftConfiguration = new SwerveAndSteerModuleConfig.Builder()
      .withDriveCanId(REAR_LEFT_DRIVE_CAN_ID)
      .withTurnPwmPort(REAR_LEFT_TURN_PWM_PORT)
      .withTurnAbsEncAnalogPortAndOffset(REAR_LEFT_TURN_ABS_ENC_PORT, REAR_LEFT_TURN_ABS_ENC_OFFSET)
      .withRelEncDioPorts(REAR_LEFT_TURN_REL_ENC_PORT_A, REAR_LEFT_TURN_REL_ENC_PORT_B)
      .withTurnPIDConstants(new PIDConstants(TURN_P, TURN_I, TURN_D))
      .withDrivePIDConstants(new PIDConstants(DRIVE_P, DRIVE_I, DRIVE_D, DRIVE_F))
      .build();
    ISwerveModuleConfiguration rearRightConfiguration = new SwerveAndSteerModuleConfig.Builder()
      .withDriveCanId(REAR_RIGHT_DRIVE_CAN_ID)
      .withTurnPwmPort(REAR_RIGHT_TURN_PWM_PORT)
      .withTurnAbsEncAnalogPortAndOffset(REAR_RIGHT_TURN_ABS_ENC_PORT, REAR_RIGHT_TURN_ABS_ENC_OFFSET)
      .withRelEncDioPorts(REAR_RIGHT_TURN_REL_ENC_PORT_A, REAR_RIGHT_TURN_REL_ENC_PORT_B)
      .withTurnPIDConstants(new PIDConstants(TURN_P, TURN_I, TURN_D))
      .withDrivePIDConstants(new PIDConstants(DRIVE_P, DRIVE_I, DRIVE_D, DRIVE_F))
      .build();
    
    mModules = new SwerveModule[] { 
      new SwerveModule("FL", frontLeftConfiguration),
      new SwerveModule("FR", frontRightConfiguration),
      new SwerveModule("RL", rearLeftConfiguration),
      new SwerveModule("RR", rearRightConfiguration)
    };

    mKinematics = new SwerveDriveKinematics(
      new Translation2d(Constants.SWERVE_LOC_Y_METERS, Constants.SWERVE_LOC_X_METERS),
      new Translation2d(Constants.SWERVE_LOC_Y_METERS, -Constants.SWERVE_LOC_X_METERS),
      new Translation2d(-Constants.SWERVE_LOC_Y_METERS, Constants.SWERVE_LOC_X_METERS),
      new Translation2d(-Constants.SWERVE_LOC_Y_METERS, -Constants.SWERVE_LOC_X_METERS));
    mGyro = new AHRS();
    reset();
    mOdometry = new SwerveDriveOdometry(mKinematics, Rotation2d.fromDegrees(getHeading()));
  }

  @Override
  public void periodic() {
    mOdometry.update(Rotation2d.fromDegrees(getHeading()), getModuleStates());
    for(SwerveModule module : mModules) {
      module.outputDebug();
    }
  
    SmartDashboard.putNumber("Heading (Deg)", getHeading());
  }

  @Override
  public void simulationPeriodic() {
    // This method will be called once per scheduler run during simulation
  }

  public void driveRobotOriented(double pMetersPerSecondFwd, double pMetersPerSecondSide, double pRadiansPerSecondTurn, boolean pIsOpenLoop) {
    ChassisSpeeds chassisSpeeds = new ChassisSpeeds(pMetersPerSecondFwd, pMetersPerSecondSide, pRadiansPerSecondTurn);
    SwerveModuleState[] states = mKinematics.toSwerveModuleStates(chassisSpeeds);
    setModuleStates(states, pIsOpenLoop);
  }

  public void setModuleStates(SwerveModuleState[] pStates, boolean pIsOpenLoop) {
    SwerveDriveKinematics.desaturateWheelSpeeds(pStates, Constants.MAXIMUM_SPEED_METERS_PER_SECONDS);                   
    for(int i = 0; i < mModules.length; i++) {
      mModules[i].setState(pStates[i], pIsOpenLoop);
    }
  }

  public void setModuleStates(SwerveModuleState[] pStates) {
    setModuleStates(pStates, false);
  }

  public SwerveModuleState[] getModuleStates() {
    SwerveModuleState[] states = new SwerveModuleState[mModules.length];
    for(int i = 0; i < mModules.length; i++) {
      states[i] = mModules[i].getState();
    }
    return states;
  }

  public void driveFieldOriented(double pMetersPerSecondFwd, double pMetersPerSecondSide, double pRadiansPerSecondTurn, boolean pIsOpenLoop) {
    ChassisSpeeds translatedSpeeds = 
        ChassisSpeeds.fromFieldRelativeSpeeds(pMetersPerSecondFwd, pMetersPerSecondSide, pRadiansPerSecondTurn, Rotation2d.fromDegrees(getHeading()));
    driveRobotOriented(translatedSpeeds.vxMetersPerSecond, translatedSpeeds.vyMetersPerSecond, translatedSpeeds.omegaRadiansPerSecond, pIsOpenLoop);
  }

  public void reset() {
    for(SwerveModule module : mModules) {
      module.reset();
    }
    mGyro.reset();
  }

  public SwerveDriveKinematics getSwerveKinematics() {
    return mKinematics;
  }

  public Pose2d getPose() {
    return mOdometry.getPoseMeters();
  }

  public void resetOdometry(Pose2d pPose) {
    mOdometry.resetPosition(pPose, Rotation2d.fromDegrees(getHeading()));
  }

  public double getHeading() {
    double gyroAngle = -mGyro.getAngle() * GYRO_ERROR_FACTOR;
    return (gyroAngle % 360 + 360) % 360;
  }
}
