// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final double ksVolts = 0.91724;
    public static final double kvVoltSecondsPerMeter = 2.9133;
    public static final double kaVoltSecondsSquaredPerMeter = 0.82423;
    public static final double kMaxSpeedMetersPerSecond = 3.0;
    public static final double kMaxAccelerationMetersPerSecondSquared = 1.0;
    public static final double kPDriveVel = 4.5373;
    public static double GYRO_P = 0.045;
    public static double GYRO_I = 0.0;
    public static double GYRO_D = 0.0035;
    public static final double kTrackwidthMeters = 1.55;
    public static final DifferentialDriveKinematics kDriveKinematics =
        new DifferentialDriveKinematics(kTrackwidthMeters);
    public static final int kEncoderCPR = 1440;
    public static final double kRatio = 1.0;
    public static final double kWheelDiameterMeters = 0.1524;
    public static final double kEncoderDistancePerPulse =
        // Assumes the encoders are directly mounted on the wheel shafts
        1.047*((kWheelDiameterMeters * Math.PI) / ((double) kEncoderCPR * kRatio));
}
