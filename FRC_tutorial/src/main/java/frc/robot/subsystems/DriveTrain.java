// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class DriveTrain extends SubsystemBase {

 // DifferentialDrive driveTrain;

  private final WPI_TalonSRX leftDrive;

  private final WPI_TalonSRX rightDrive;

  private final AHRS gyro;

  // Odometry class for tracking robot pose
  private final DifferentialDriveOdometry odometry;
  
  /** Creates a new DriveTrain. */
  public DriveTrain() {
    leftDrive = new WPI_TalonSRX(0);
    rightDrive = new WPI_TalonSRX(1);
    gyro = new AHRS();
    odometry = new DifferentialDriveOdometry(Rotation2d.fromDegrees(gyro.getAngle()));
  }

  public void driveArcade(double speed, double rotation) {
    double leftSpeed = MathUtil.clamp(speed + rotation, -1.0, 1.0);
    double rightSpeed = -MathUtil.clamp(speed - rotation, -1.0, 1.0);
    double leftSign =  leftSpeed > 0 ? 1 : -1;
    double rightSign = rightSpeed > 0 ? 1 : -1;
    leftSpeed = leftSign * Math.abs(leftSpeed * leftSpeed);
    rightSpeed = rightSign * Math.abs(rightSpeed * rightSpeed);
    leftDrive.set(leftSpeed);
    rightDrive.set(rightSpeed);
  }

  public void driveTankVolts(double left, double right) {
    leftDrive.setVoltage(left);
    rightDrive.setVoltage(-right);
  }

  @Override
  public void periodic() {
    SmartDashboard.putData(gyro);
    SmartDashboard.putNumber("Left Distance", getLeftDistance());
    SmartDashboard.putNumber("Right Encoder", getRightDistance());
    odometry.update(
      gyro.getRotation2d(), getLeftDistance(), getRightDistance());
    SmartDashboard.putNumber("Odom X", odometry.getPoseMeters().getX());
    SmartDashboard.putNumber("Odom Y", odometry.getPoseMeters().getY());
    SmartDashboard.putNumber("Odom Rotation", odometry.getPoseMeters().getRotation().getDegrees());
  }

  public double getHeading() {
    return gyro.getAngle();
  }

  public void resetSensors() {
    gyro.reset();
    resetEncoders();
    odometry.resetPosition(new Pose2d(), new Rotation2d());
  }

  public void resetEncoders() {
    leftDrive.setSelectedSensorPosition(0);
    rightDrive.setSelectedSensorPosition(0);
  }

  private double getLeftDistance() {
    return leftDrive.getSelectedSensorPosition() * Constants.kEncoderDistancePerPulse;
  }

  private double getRightDistance() {
    return -rightDrive.getSelectedSensorPosition() * Constants.kEncoderDistancePerPulse;
  }

  private double getLeftVelocity() {
    return leftDrive.getSelectedSensorVelocity() * Constants.kEncoderDistancePerPulse * 10;
  }

  private double getRightVelocity() {
    return -rightDrive.getSelectedSensorVelocity() * Constants.kEncoderDistancePerPulse * 10;
  }

  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(getLeftVelocity(), getRightVelocity());
  }

  public void resetOdometry(Pose2d pose) {
    resetEncoders();
    odometry.resetPosition(pose, gyro.getRotation2d());
  }

  public Pose2d getPose() {
    return odometry.getPoseMeters();
  }
}
