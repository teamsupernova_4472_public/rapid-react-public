// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;

/** An DriveTrain command that uses an DriveTrain subsystem. */
public class DriveTrainCommand extends CommandBase {

  private final DriveTrain m_subsystem;

  private final XboxController controller;

  private final PIDController pidController;

  private double targetHeading;

  /**
   * Creates a new DriveTrainCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public DriveTrainCommand(DriveTrain subsystem) {
    m_subsystem = subsystem;
    controller = new XboxController(0);
    pidController = new PIDController(Constants.GYRO_P, Constants.GYRO_I, Constants.GYRO_D);
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {
    m_subsystem.resetSensors();
    m_subsystem.resetOdometry(new Pose2d());
    targetHeading = 0;
  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    drive(controller);
  }

  public void drive(XboxController controller) {
    double leftY = -controller.getLeftY();
    double rightX = controller.getRightX();
    double debandSpeed = MathUtil.applyDeadband(leftY,  0.1);
    double debandRot = MathUtil.applyDeadband(rightX, 0.1);
    // double currentHeading = m_subsystem.getHeading(); 
    // if(debandRot == 0) {
    //   debandRot = pidController.calculate(currentHeading, targetHeading);
    //   debandRot = MathUtil.clamp(debandRot,  -1.0, 1.0);
    // } else {
    //   targetHeading = m_subsystem.getHeading();
    // }
    m_subsystem.driveArcade(debandSpeed, debandRot);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
