package frc.robot.sensors;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.Flywheel;
import frc.robot.commands.FlywheelTeleOp;
import edu.wpi.first.wpilibj.PowerDistribution;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;


public class SensorsSmartDashboard {

    DriveTrain driveTrain;
    // BallIntake ballIntake;
    // Climber climber;
    Indexer indexer;
    PowerDistribution pd;
    Flywheel flywheel;
    FlywheelTeleOp flywheelControl;
 

    public SensorsSmartDashboard(DriveTrain p_driveTrain,
    // BallIntake p_ballIntake, 
    // Climber p_climber, 
    Indexer p_indexer,
                               // PowerDistribution p_pd//, 
    Flywheel p_flywheel,
    FlywheelTeleOp p_flywheelControl){
        driveTrain = p_driveTrain;
        // ballIntake = p_ballIntake;
        // climber = p_climber;
        indexer = p_indexer;
        //pd= p_pd;
        flywheel = p_flywheel;
        flywheelControl = p_flywheelControl;

    }
    public void outputSensors(){

        // double leftPosition = driveTrain.leftDrive.getSensorCollection().getIntegratedSensorPosition();
        // double leftVelocity = driveTrain.leftDrive.getSensorCollection().getIntegratedSensorVelocity();
        // double rightPosition = driveTrain.rightDrive.getSensorCollection().getIntegratedSensorPosition();
        // double rightVelocity = driveTrain.rightDrive.getSensorCollection().getIntegratedSensorVelocity();
        // Value solenoidState = ballIntake.solenoid.get();
        // double intakeValue = ballIntake.intake.get();
        // double straightClimbPosition = climber.getStraightClimbPosition();
        // double angleClimbPosition = climber.getAngleClimbPosition();
        double indexValue = indexer.index.get();
        double flywheelVelocity = flywheel.flywheelTwo.getSensorCollection().getIntegratedSensorVelocity();

        // SmartDashboard.putNumber("Left position", leftPosition);
        // SmartDashboard.putNumber("Right position", rightPosition);
        // SmartDashboard.putNumber("Left velocity", leftVelocity);
        // SmartDashboard.putNumber("Right Velocity", rightVelocity);
        // SmartDashboard.putString("Solenoid state", solenoidState.toString());
        // SmartDashboard.putNumber("Intake Value", intakeValue);
        // SmartDashboard.putNumber("Index Value", indexValue);
        // SmartDashboard.putNumber("Straight climb position", straightClimbPosition);
        // SmartDashboard.putNumber("Angle climb position", angleClimbPosition);
        SmartDashboard.putNumber("Current Flywheel Speed", flywheelVelocity);
        //SmartDashboard.putData("Power Distribution", pd);
        SmartDashboard.putData("Gyro yaw", driveTrain.gyro);
        SmartDashboard.putNumber("Flywheel Voltage", flywheel.getVoltage());
        SmartDashboard.putNumber("Target Flywheel speed", flywheelControl.getTargetSpeed());
    }




    
}
