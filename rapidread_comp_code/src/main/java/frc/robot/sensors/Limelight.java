package frc.robot.sensors;

import edu.wpi.first.networktables.NetworkTable;
import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import static frc.robot.Constants.*;

public class Limelight extends SubsystemBase{
    
    private final NetworkTableEntry deltaXDeg;
    private final NetworkTableEntry deltaYDeg;
    private final NetworkTableEntry targetAPer;
    private final NetworkTableEntry ledMode;
    private final NetworkTableEntry camMode;    

    public Limelight(){
        NetworkTable table = NetworkTableInstance.getDefault().getTable(LIMETABLE);
        deltaXDeg = table.getEntry("tx");
        deltaYDeg = table.getEntry("ty");
        targetAPer = table.getEntry("ta");
        ledMode = table.getEntry("ledMode");
        camMode = table.getEntry("camMode");
    }

    public double getXDegreesAwayFromTarget() {
        return deltaXDeg.getDouble(0.0);
    }

    public double getYDegreesAwayFromTarget() {
        return deltaYDeg.getDouble(0.0);
    }

    public double getTargetAreaPercent() {
        return targetAPer.getDouble(0.0);
    }

    public void setLedMode(int value) {
        ledMode.setNumber(value);
    }
    
    public void setCamMode(int value) {
        camMode.setNumber(value);
    }

}
