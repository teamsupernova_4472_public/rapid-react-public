package frc.robot.sensors;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import edu.wpi.first.wpilibj.Ultrasonic;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class UltrasonicSensor extends SubsystemBase
{
    final Ultrasonic ultra;
    final double minDistance;
    final double maxDistance;

    public UltrasonicSensor(int port1, int port2, double minDistance, double maxDistance)
    {
        ultra = new Ultrasonic(port1, port2);
        Ultrasonic.setAutomaticMode(true);
        this.minDistance = minDistance;
        this.maxDistance = maxDistance;
    }

    public boolean isBlocked()
    {
        double rangeInches = ultra.getRangeInches();
        if(rangeInches < minDistance || rangeInches > maxDistance)
        {
            return true;
        }

        return false;
    }

    @Override
    public void periodic() {
        SmartDashboard.putNumber("Distance", ultra.getRangeInches());
    }
}
