// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import javax.management.ValueExp;

import edu.wpi.first.math.kinematics.DifferentialDriveKinematics;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;

/**
 * The Constants class provides a convenient place for teams to hold robot-wide numerical or boolean
 * constants. This class should not be used for any other purpose. All constants should be declared
 * globally (i.e. public static). Do not put anything functional in this class.
 *
 * <p>It is advised to statically import this class (or one of its inner classes) wherever the
 * constants are needed, to reduce verbosity.
 */
public final class Constants {
    public static final double QUART_A = -0.0076;
    public static final double QUART_B = 0.0147;
    public static final double QUART_C = 6.6391;
    public static final double QUART_D = -189.15;
    public static final double QUART_E = 10729;
    public static final int PDB_PORT = 0;
    public static final int PCM_PORT = 1;
    public static final int LEFTDRIVE_PORT = 2;
    public static final int LEFTDRIVE2_PORT = 3;
    public static final int RIGHTDRIVE_PORT = 4;
    public static final int RIGHTDRIVE2_PORT = 5;
    public static final int STRAIGHT_CLIMBER0_PORT = 11;
    public static final int STRAIGHT_CLIMBER1_PORT = 12;
    public static final int INTAKE_FLIPOUT_PORT = 13;
    public static final int INTAKE_PORT = 6;
    public static final int INDEXER_PORT = 7;
    public static final int CONVEYOR = 8;
    public static final int FLYWHEEL1_PORT = 9;
    public static final int FLYWHEEL2_PORT = 10;
    public static final int RUNG_TRAVERSAL_PORT = 6; // theres probably more motors
    public static final int PNEM_FORWARD_HOOD_PORT = 3;
    public static final int PNEN_REVERSE_HOOD_PORT = 4;
    public static final int PNEM_FORWARD_CLIMBER_PORT = 1;
    public static final int PNEN_REVERSE_CLIMBER_PORT = 2;
    public static final int DEFAULT_TIMEOUT = 100;
    public static final int ULTRASONIC_PORT1 = 1;
    public static final int ULTRASONIC_PORT2 = 0;
    public static final int INTAKE_DEPLOY_TIME_SECONDS = 1;
    public static final double TWO_BALL_FLYWHEEL_SPEED = 9789.0;
    public static final double MID_TWO_BALL_FLYWHEEL_SPEED = 9881.0;
    public static final double FOUR_BALL_FLYWHEEL_SPEED = 12100.0;
    public static final double ULTRASONIC_BALL_MIN_DISTANCE = 3.5; //Test this
    public static final double ULTRASONIC_BALL_MAX_DISTANCE = 20.0; //Test this
    public static final double HIGH_ACTUATOR_POSITION = 0.25;
    public static final double LOW_ACTUATOR_POSITION = 0.75;
    public static final double MAX_FLY_WHEEL_SPEED = 15600;
    public static final double ENCODER_TICKS_PER_INCH = 1326;
    public static final double CLIMBER_LEFT_UPPER_LIMIT = 113.0;
    public static final double CLIMBER_RIGHT_UPPER_LIMIT = 114.0;
    public static final double DRIVE_P = 0.000015;
    public static final double DRIVE_I = 0;
    public static final double DRIVE_D = 0.00000010;
    public static final double LIMELIGHT_P = 0.025;
    public static final double LIMELIGHT_I = 0;
    public static final double LIMELIGHT_D = 0.0035;
    public static final double DEFAULT_INTAKE_SPEED = 1.0;
    public static final double DEFAULT_INTAKE_VOLTS = 7.0;
    public static final double DEFAULT_CONVEYOR_SPEED = 0.75;
    public static final double DEFAULT_CONVEYOR_VOLTS = 8.25;
    public static final double DEFAULT_INDEXER_SPEED = 0.5;
    public static final double DEFAULT_INDEXER_VOLTS = 1.65;
    public static final double GYRO_P = 0.015;
    public static final double GYRO_I = 0.0;
    public static final double GYRO_D = 0.001;
    public static final String LIMETABLE = "limelight-nova";
    public static final String PHOTONTABLE = "photonvision-nova";
    public static final double DEPLOY_INTAKE_VOLTS = 3.0;
    public static final double DEPLOY_INTAKE_POS = 9.5;
    public static final double RETRACT_INTAKE_POS = 1.0;
    public static final Value DEPLOY_INTAKE_STATE = Value.kReverse;
    
    public static final double kTrackwidthMeters = .70;
    public static final DifferentialDriveKinematics kDriveKinematics =
        new DifferentialDriveKinematics(kTrackwidthMeters);

    public static final int kEncoderCPR = 2048;
    public static final double kRatio = 8.5;
    public static final double kWheelDiameterMeters = 0.102;
    public static final double kEncoderTicksPerRev = 17074.5;
    public static final double kEncoderDistancePerPulse = (0.102 * Math.PI) / kEncoderTicksPerRev;


    // These are example values only - DO NOT USE THESE FOR YOUR OWN ROBOT!
    // These characterization values MUST be determined either experimentally or theoretically
    // for *your* robot's drive.
    // The Robot Characterization Toolsuite provides a convenient tool for obtaining these
    // values for your robot.
    public static final double ksVolts = 0.68007;
    public static final double kvVoltSecondsPerMeter = 2.741;
    public static final double kaVoltSecondsSquaredPerMeter = 0.39676;

    // Example value only - as above, this must be tuned for your drive!
    public static final double kPDriveVel = 3.7818;

    public static final double kMaxSpeedMetersPerSecond = 3;
    public static final double kMaxAccelerationMetersPerSecondSquared = 2;

    // Reasonable baseline values for a RAMSETE follower in units of meters and seconds
    public static final double kRamseteB = 2;
    public static final double kRamseteZeta = 0.7;
}
