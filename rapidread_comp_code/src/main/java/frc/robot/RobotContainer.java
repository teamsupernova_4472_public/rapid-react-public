// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot;

import java.util.ArrayList;

import com.ctre.phoenix.motorcontrol.can.TalonFX;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.GenericHID;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.I2C.Port;
import edu.wpi.first.wpilibj.PowerDistribution.ModuleType;
import edu.wpi.first.wpilibj.XboxController.Button;
import edu.wpi.first.wpilibj.shuffleboard.BuiltInWidgets;
import edu.wpi.first.wpilibj.shuffleboard.Shuffleboard;
import edu.wpi.first.wpilibj.shuffleboard.ShuffleboardTab;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj.smartdashboard.SendableChooser;
import frc.robot.commands.BallIntakeFlipOutAuton;
import frc.robot.commands.BallIntakeFlipOutTeleOp;
import frc.robot.commands.BallIntakeFlipOutTeleOpTester;
import frc.robot.commands.BallIntakeTeleOp;
import frc.robot.commands.ClimbTeleOp;
import frc.robot.commands.ConveyorTeleop;
import frc.robot.commands.DriveStraight;
import frc.robot.commands.DriveTrainMotionProfileCommand;
import frc.robot.commands.DriveTrainTeleOp;
import frc.robot.commands.FlywheelTeleOp;
import frc.robot.commands.IndexerTeleOp;
import frc.robot.commands.LimelightTurn;
import frc.robot.commands.LinearHoodTeleOp;
import frc.robot.commands.Turn;
import frc.robot.commands.Autopaths.TwoBallAuto;
import frc.robot.commands.Autopaths.ExampleRamsetePath;
import frc.robot.commands.Autopaths.TerminalFourBallAuto;
import frc.robot.commands.Autopaths.HangarFourBallAuto;
import frc.robot.commands.Autopaths.MidFourBallAuto;
import frc.robot.commands.Autopaths.MidTwoBallAuto;
import frc.robot.sensors.Limelight;
import frc.robot.sensors.SensorsSmartDashboard;
import frc.robot.sensors.UltrasonicSensor;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.BallIntakeFlipOut;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Climber;
import frc.robot.subsystems.Conveyor;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.LinearHood;
import edu.wpi.first.wpilibj2.command.Command;
import edu.wpi.first.wpilibj2.command.CommandScheduler;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import edu.wpi.first.wpilibj2.command.button.JoystickButton;
import edu.wpi.first.wpilibj.PowerDistribution;

/**
 * This class is where the bulk of the robot should be declared. Since Command-based is a
 * "declarative" paradigm, very little robot logic should actually be handled in the {@link Robot}
 * periodic methods (other than the scheduler calls). Instead, the structure of the robot (including
 * subsystems, commands, and button mappings) should be declared here.
 */
public class RobotContainer {
  // The robot's subsystems and commands are defined here...
  final Flywheel flyWheel = new Flywheel();
  final DriveTrain driveTrain = new DriveTrain();
  final PowerDistribution pd = new PowerDistribution(Constants.PDB_PORT, ModuleType.kCTRE);
  final Limelight limelight = new Limelight();
  //  final Compressor compressor = new Compressor(Constants.PCM_PORT, PneumaticsModuleType.CTREPCM);
  final BallIntake ballIntake = new BallIntake();
  final Climber climber = new Climber();
  final BallIntakeFlipOut flipOut = new BallIntakeFlipOut();
  final UltrasonicSensor ultra = new UltrasonicSensor(Constants.ULTRASONIC_PORT1, Constants.ULTRASONIC_PORT2, 
                                                      Constants.ULTRASONIC_BALL_MIN_DISTANCE, Constants.ULTRASONIC_BALL_MAX_DISTANCE);
  final Indexer indexer = new Indexer(ultra);
  final Conveyor conveyor = new Conveyor(ultra);
  final LinearHood hood = new LinearHood();
  final LimelightTurn limelightTurn = new LimelightTurn(driveTrain, limelight, Integer.MAX_VALUE);

  final XboxController driverController = new XboxController(0);
  final XboxController partnerController = new XboxController(1);
  final Command ramseteCommmand = new ExampleRamsetePath(driveTrain);
  private final DriveTrainTeleOp driveTrainCommand = new DriveTrainTeleOp(driveTrain, driverController);
  private final IndexerTeleOp indexerCommand = new IndexerTeleOp(indexer, driverController, partnerController);
  private final FlywheelTeleOp flywheelCommand = new FlywheelTeleOp(flyWheel, 0.5 * Constants.MAX_FLY_WHEEL_SPEED, driverController, partnerController, limelight);
  private final LinearHoodTeleOp linearHoodCommand = new LinearHoodTeleOp(hood, partnerController);
  private final ConveyorTeleop conveyorCommand = new ConveyorTeleop(driverController, partnerController, conveyor);
  private final BallIntakeTeleOp ballIntakeCommand = new BallIntakeTeleOp(ballIntake, driverController, partnerController);
  private final ClimbTeleOp climbCommand = new ClimbTeleOp(climber, partnerController);
  private final BallIntakeFlipOutTeleOp flipCommand = new BallIntakeFlipOutTeleOp(flipOut, driverController, partnerController);
  private final Command twoBallAuto = new TwoBallAuto(driveTrain, ballIntake, flipOut, indexer, hood, flyWheel, conveyor, limelight);
  private final Command midTwoBallAuto = new MidTwoBallAuto(driveTrain, ballIntake, flipOut, indexer, hood, flyWheel, conveyor, limelight);
  private final TerminalFourBallAuto terminalFourBallAuto = new TerminalFourBallAuto(driveTrain, ballIntake, flipOut, indexer, hood, flyWheel, conveyor, limelight);
  private final HangarFourBallAuto hangarFourBallAuto = new HangarFourBallAuto(driveTrain, ballIntake, flipOut, indexer, hood, flyWheel, conveyor, limelight);
  private final MidFourBallAuto midFourBallAuto = new MidFourBallAuto(driveTrain, ballIntake, flipOut, indexer, hood, flyWheel, conveyor, limelight);

  private final SendableChooser<Command> chooser;
  final SensorsSmartDashboard dashboard = new SensorsSmartDashboard(driveTrain, 
  // ballIntake, 
  // climber, 
  indexer,
  // pd, 
  flyWheel,
  flywheelCommand
  );
  /* 
  private final Command autoCommand = new AutonDriveBackShoot(driveTrain, ballIntake, indexer, hood, flyWheel);
*/
  /** The container for the robot. Contains subsystems, OI devices, and commands. */
  public RobotContainer() {
    CommandScheduler.getInstance().setDefaultCommand(driveTrain,driveTrainCommand);
    CommandScheduler.getInstance().setDefaultCommand(indexer, indexerCommand);
    CommandScheduler.getInstance().setDefaultCommand(flyWheel, flywheelCommand);
    CommandScheduler.getInstance().setDefaultCommand(hood, linearHoodCommand);
    CommandScheduler.getInstance().setDefaultCommand(conveyor, conveyorCommand);
    CommandScheduler.getInstance().setDefaultCommand(ballIntake, ballIntakeCommand);
    CommandScheduler.getInstance().setDefaultCommand(climber, climbCommand);
    CommandScheduler.getInstance().setDefaultCommand(flipOut, flipCommand);
    
    chooser = new SendableChooser<Command>();

    ShuffleboardTab drivingTab = Shuffleboard.getTab("Driving");
    chooser.setDefaultOption("2 Ball Auto", twoBallAuto);
    chooser.setDefaultOption("Middle 2 Ball Auto", midTwoBallAuto);
    chooser.addOption("Hangar side 4 Ball Auto", hangarFourBallAuto);
    chooser.addOption("Middle 4 Ball Auto", midFourBallAuto);
    chooser.addOption("Terminal side 4 Ball Auto", terminalFourBallAuto);
    drivingTab.add("Autonomous Selection", chooser).withSize(4, 2).withPosition(0, 2).withWidget(BuiltInWidgets.kSplitButtonChooser);
    /*
    
   */ // Configure the button bindings
    configureButtonBindings();
  }

  /**
   * Use this method to define your button->command mappings. Buttons can be created by
   * instantiating a {@link GenericHID} or one of its subclasses ({@link
   * edu.wpi.first.wpilibj.Joystick} or {@link XboxController}), and then passing it to a {@link
   * edu.wpi.first.wpilibj2.command.button.JoystickButton}.
   */
  private void configureButtonBindings() {
    JoystickButton limelightTurnButton = new JoystickButton(driverController, Button.kA.value);
    limelightTurnButton.whenHeld(limelightTurn);
  }

  /**
   * Use this to pass the autonomous command to the main {@link Robot} class.
   *
   * @return the command to run in autonomous
   */
  public Command getAutonomousCommand() {
    // An DriveTrainCommand will run in autonomous
    //return autoCommand;
    return chooser.getSelected();
    //return new WaitCommand(0);
  }
  public void outputSensors(){
    dashboard.outputSensors();
  }
}
