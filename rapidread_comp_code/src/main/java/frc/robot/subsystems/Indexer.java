package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.sensors.UltrasonicSensor;

/*
Implement an indexer (a mechanism to move balls from robot's "containment area" into the flywheel) 
*/


public class Indexer extends SubsystemBase
{
    public final WPI_TalonSRX index; // motor for index, talon
    private final UltrasonicSensor ultra;


    public Indexer(UltrasonicSensor ultrasonic) // constructor
    {
        index = new WPI_TalonSRX(Constants.INDEXER_PORT); // new motor object, assigns it a port
        ultra = ultrasonic;
        SubsystemUtils.setCurrentLimits(index, 15);
    }

    public void indexerMove(double speed) // method to run the indexer
    {
        index.set(-speed); // sets indexer speed to formal argument double variable
    }
    public void indexerMoveVolts(double voltage)
    {
        index.setVoltage(-voltage);
    }

    public void indexerDownWithUltra() {
        if(!ultra.isBlocked()) {
            indexerMoveVolts(-Constants.DEFAULT_INDEXER_VOLTS);
        } else {
            indexerMove(0.0);
        }
    }
}