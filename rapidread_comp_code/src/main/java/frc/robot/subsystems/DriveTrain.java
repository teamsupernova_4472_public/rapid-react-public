// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;

import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import static frc.robot.Constants.*;

public class DriveTrain extends SubsystemBase {

  public WPI_TalonFX leftDrive;
  WPI_TalonFX leftDrive2;
  public WPI_TalonFX rightDrive;
  WPI_TalonFX rightDrive2;

  public AHRS gyro = new AHRS();

  // Odometry class for tracking robot pose
  private final DifferentialDriveOdometry odometry;

  PIDController gyroPID = new PIDController(GYRO_P, GYRO_I, GYRO_D);
  PIDController drivePID = new PIDController(DRIVE_P, DRIVE_I, DRIVE_D);
  
  /** Creates a new DriveTrain. */
  public DriveTrain() 
  {
    leftDrive = new WPI_TalonFX(LEFTDRIVE_PORT);
    leftDrive2 = new WPI_TalonFX(LEFTDRIVE2_PORT);
    rightDrive = new WPI_TalonFX(RIGHTDRIVE_PORT);
    rightDrive2 = new WPI_TalonFX(RIGHTDRIVE2_PORT);
    SubsystemUtils.setCurrentLimits(leftDrive, 30, 35, 0.1);
    SubsystemUtils.setCurrentLimits(leftDrive2, 30, 35, 0.1);
    SubsystemUtils.setCurrentLimits(rightDrive, 30, 35, 0.1);
    SubsystemUtils.setCurrentLimits(rightDrive2, 30, 35, 0.1);
    resetEncoders();
    odometry = new DifferentialDriveOdometry(Rotation2d.fromDegrees(gyro.getAngle()));
  }

  private double getLeftDistance() {
    return -leftDrive.getSelectedSensorPosition() * kEncoderDistancePerPulse;
  }

  private double getRightDistance() {
    return rightDrive.getSelectedSensorPosition() * kEncoderDistancePerPulse;
  }

  private double getLeftVelocity() {
    return -leftDrive.getSelectedSensorVelocity() * kEncoderDistancePerPulse * 10;
  }

  private double getRightVelocity() {
    return rightDrive.getSelectedSensorVelocity() * kEncoderDistancePerPulse * 10;
  }

  @Override
  public void periodic() {
    // Update the odometry in the periodic block
    odometry.update(
        gyro.getRotation2d(), getLeftDistance(), getRightDistance());

    SmartDashboard.putNumber("Odom X", odometry.getPoseMeters().getX());
    SmartDashboard.putNumber("Odom Y", odometry.getPoseMeters().getY());
    SmartDashboard.putNumber("Odom Rotation", odometry.getPoseMeters().getRotation().getDegrees());
    SmartDashboard.putNumber("Left speed", getLeftVelocity());
    SmartDashboard.putNumber("Right speed", getRightVelocity());

    SmartDashboard.putNumber("Left Encoder", -leftDrive.getSelectedSensorPosition());
    SmartDashboard.putNumber("Right Encoder", rightDrive.getSelectedSensorPosition());
    SmartDashboard.putNumber("Gyro Val", gyro.getAngle());
  }

  /**
   * Returns the currently-estimated pose of the robot.
   *
   * @return The pose.
   */
  public Pose2d getPose() {
    return odometry.getPoseMeters();
  }

  /**
   * Returns the current wheel speeds of the robot.
   *
   * @return The current wheel speeds.
   */
  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(getLeftVelocity(), getRightVelocity());
  }

  /**
   * Resets the odometry to the specified pose.
   *
   * @param pose The pose to which to set the odometry.
   */
  public void resetOdometry(Pose2d pose) {
    resetEncoders();
    odometry.resetPosition(pose, gyro.getRotation2d());
  }


  public void drive(double output){
      driveTank(output, output);
  }

  public void driveArcade(double output, double turn){
    double leftOutput = output + turn, rightOutput = output - turn;
    driveTank(leftOutput, rightOutput);
  }

  public void driveTank(double left, double right){
    leftDrive.set(-left);
    leftDrive2.set(-left);
    rightDrive.set(right);
    rightDrive2.set(right);
  }

  public void turn(double output)
  {
    leftDrive.set(output);
    leftDrive2.set(output);
    rightDrive.set(output);
    rightDrive2.set(output);
  }

  public void turnVolts(double voltage)
  {
    leftDrive.setVoltage(voltage);
    leftDrive2.setVoltage(voltage);
    rightDrive.setVoltage(voltage);
    rightDrive2.setVoltage(voltage);
  }

  public void driveTankVolts(double leftVoltage, double rightVoltage)
  {
    leftDrive.setVoltage(-leftVoltage);
    leftDrive2.setVoltage(-leftVoltage);
    rightDrive.setVoltage(rightVoltage);
    rightDrive2.setVoltage(rightVoltage);
  }

  public void driveArcadeVolts(double outputVoltage, double turnVoltage){
    double leftOutput = outputVoltage + turnVoltage, rightOutput = outputVoltage - turnVoltage;
    driveTankVolts(leftOutput, rightOutput);
  }

  public void resetEncoders() {
    leftDrive.setSelectedSensorPosition(0);
    rightDrive.setSelectedSensorPosition(0);
  }

  /**
   * Gets the average distance of the two encoders.
   *
   * @return the average of the two encoder readings
   */
  public double getAverageEncoderDistance() {
    return (getLeftDistance() + getRightDistance()) / 2.0;
  }


  /** Zeroes the heading of the robot. */
  public void zeroHeading() {
    gyro.reset();
  }

  /**
   * Returns the heading of the robot.
   *
   * @return the robot's heading in degrees, assuming positive values are clockwise.
   */
  public double getHeading() {
    return gyro.getAngle();
  }

  /**
   * Returns the turn rate of the robot.
   *
   * @return The turn rate of the robot, in degrees per second, assuming positive values are clockwise.
   */
  public double getTurnRate() {
    return -gyro.getRawGyroZ();
  }
}
