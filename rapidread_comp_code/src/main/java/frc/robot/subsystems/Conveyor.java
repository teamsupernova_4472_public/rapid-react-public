package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.sensors.UltrasonicSensor;

public class Conveyor extends SubsystemBase{
    final WPI_TalonSRX conveyor;
    private final UltrasonicSensor ultra;

    public Conveyor(UltrasonicSensor ultrasonic)
    {
        conveyor = new WPI_TalonSRX(Constants.CONVEYOR);
        ultra = ultrasonic;
        SubsystemUtils.setCurrentLimits(conveyor, 15);
    }

    public void setSpeed(double speed)
    {
        conveyor.set(speed);
    }

    public void setVolts(double voltage)
    {
        conveyor.setVoltage(voltage);
    }

    public void conveyorUpWithUltra() {
        if(!ultra.isBlocked()) {
            setVolts(-Constants.DEFAULT_CONVEYOR_VOLTS);
        } else {
            setSpeed(0.0);
        }
    }

}
