package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;
import frc.robot.sensors.Limelight;

public class Flywheel extends SubsystemBase
{
    public WPI_TalonFX flywheelOne;
    public WPI_TalonFX flywheelTwo;

    public Flywheel()
    {
        this.flywheelOne = new WPI_TalonFX(Constants.FLYWHEEL1_PORT);
        this.flywheelTwo = new WPI_TalonFX(Constants.FLYWHEEL2_PORT);
        // Set Flywheel PIDs
        flywheelOne.configClosedloopRamp(1);
        flywheelOne.selectProfileSlot(0, 0);
        flywheelOne.config_kP(0, 0.09);//.2);
        flywheelOne.config_kD(0, 0);//.1);//.000025);
        flywheelOne.config_kF(0, 0.066);//.050);
        flywheelOne.config_kI(0, 0);//.000025);
        flywheelTwo.selectProfileSlot(0, 0);
        flywheelTwo.configClosedloopRamp(1);
        flywheelTwo.config_kP(0, 0.09);//.2);
        flywheelTwo.config_kD(0, 0);//.1);
        flywheelTwo.config_kF(0, 0.066);//.050);
        flywheelOne.config_kI(0, 0);//.000025);

        flywheelOne.configVoltageCompSaturation(10);
        flywheelOne.enableVoltageCompensation(true);
        flywheelTwo.configVoltageCompSaturation(10);
        flywheelTwo.enableVoltageCompensation(true);
    }

    public double findVelocity(Limelight limelight) {
        double area = limelight.getYDegreesAwayFromTarget();
        double speed = Constants.QUART_A*Math.pow(area, 4) + Constants.QUART_B*Math.pow(area, 3) + Constants.QUART_C*Math.pow(area, 2) 
                + Constants.QUART_D*Math.pow(area, 1) + Constants.QUART_E;
        return speed;
    }

    public void setPercentage(double percentOutput) {
        flywheelOne.set(-percentOutput);
        flywheelTwo.set(percentOutput);
    }

    //Potentially needs an equation for rpm to be equivalent
    public void setSpeed(double speed)
    {
        flywheelOne.set(ControlMode.Velocity, -speed);
        flywheelTwo.set(ControlMode.Velocity, speed);
    }

    public void setVolts(double voltage)
    {
        flywheelOne.setVoltage(-voltage);
        flywheelTwo.setVoltage(voltage);
    }

    public double getVoltage()
    {
        return flywheelTwo.getMotorOutputVoltage();
    }
}
