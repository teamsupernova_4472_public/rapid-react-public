package frc.robot.subsystems;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

import frc.robot.Constants;

public class LinearHood extends SubsystemBase 
{
    private DoubleSolenoid piston;

    public LinearHood()
    {
        piston = new DoubleSolenoid(Constants.PCM_PORT,PneumaticsModuleType.CTREPCM,  //new solenoid object, type, forward & reverse deploymet
                Constants.PNEM_FORWARD_HOOD_PORT, Constants.PNEN_REVERSE_HOOD_PORT); // from constants class
    }

    public void setHoodState(Value value) // sets the intake forward or reverse, deployed
    {
        piston.set(value); // solenoid is set to the value given by something
    }

}