package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class BallIntake extends SubsystemBase
{
    public final WPI_TalonFX intake;

    public BallIntake() 
    {
        intake = new WPI_TalonFX(Constants.INTAKE_PORT); // motor
        SubsystemUtils.setCurrentLimits(intake, 20, 25, 0.1);
    }

    public void setIntakeSpeed(double speed) // sets intake speed to whatever something sets it to
    {
        intake.set(speed);
    }

    public void setIntakeVolts(double voltage)
    {
        intake.setVoltage(voltage);
    }

}
