package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.SupplyCurrentLimitConfiguration;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.ctre.phoenix.motorcontrol.can.WPI_TalonSRX;
import com.revrobotics.CANSparkMax;

public class SubsystemUtils 
{
    public static void setCurrentLimits(WPI_TalonSRX talon, int amps) {
        talon.configContinuousCurrentLimit(amps, 100);
        talon.configPeakCurrentDuration(0,100);
        talon.configPeakCurrentLimit(0, 100);
        talon.enableCurrentLimit(true);
    }
    public static void setCurrentLimits(WPI_TalonFX talon, int limitAmps, int triggerAmps, double triggerTime) {
        talon.configSupplyCurrentLimit(new SupplyCurrentLimitConfiguration(true, limitAmps, triggerAmps, triggerTime));
    }
    public static void setCurrentLimits(CANSparkMax spark, int limitAmps) {
        spark.setSmartCurrentLimit(limitAmps);
    }
}
