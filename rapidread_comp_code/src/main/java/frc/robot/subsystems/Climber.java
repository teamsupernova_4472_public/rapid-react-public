package frc.robot.subsystems;


import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.PneumaticsModuleType;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;


public class Climber extends SubsystemBase {
    //this is the straight climber motor controllers
    CANSparkMax straightClimber0 = new CANSparkMax(Constants.STRAIGHT_CLIMBER0_PORT , MotorType.kBrushless);
    CANSparkMax straightClimber1 = new CANSparkMax(Constants.STRAIGHT_CLIMBER1_PORT, MotorType.kBrushless);
    DoubleSolenoid solenoid1 = new DoubleSolenoid(Constants.PCM_PORT, PneumaticsModuleType.CTREPCM, Constants.PNEM_FORWARD_CLIMBER_PORT, Constants.PNEN_REVERSE_CLIMBER_PORT);
    /**
     * method that sets the straight climbers to the designated speed
     * @param speed 
     */
    public void straightClimb(double speed) {
        double climbSpeed0 = -speed;
        double climbSpeed1 = speed;
        straightClimber0.set(climbSpeed0);
        straightClimber1.set(climbSpeed1);
        SmartDashboard.putNumber("Climb 0", straightClimber0.getEncoder().getPosition());
        SmartDashboard.putNumber("Climb 1", straightClimber1.getEncoder().getPosition());
    }

    public void straightLeftClimb(double speed) {
        double climbSpeed0 = -speed;
        straightClimber0.set(climbSpeed0);
        SmartDashboard.putNumber("Climb 0", straightClimber0.getEncoder().getPosition());
    }

    public void straightRightClimb(double speed) {
        double climbSpeed1 = speed;
        straightClimber1.set(climbSpeed1);
        SmartDashboard.putNumber("Climb 1", straightClimber1.getEncoder().getPosition());
    }

    public void straightClimbVolts(double voltage) {
        double climbSpeed0 = -voltage;
        double climbSpeed1 = voltage;
        straightClimber0.setVoltage(climbSpeed0);
        straightClimber1.setVoltage(climbSpeed1);
        SmartDashboard.putNumber("Climb 0", straightClimber0.getEncoder().getPosition());
        SmartDashboard.putNumber("Climb 1", straightClimber1.getEncoder().getPosition());
    }

    public void angleClimbers(Value state)
    {
        solenoid1.set(state);
    }

    /**
     *  method that returns the position of the straight climbers
     * @return 
     */
    public double getStraightClimbPosition(){
        return straightClimber0.getEncoder().getPosition();
    }

    public double getLeftClimbPos()
    {
        return straightClimber0.getEncoder().getPosition();
    }

    public double getRightClimbPos()
    {
        return straightClimber1.getEncoder().getPosition();
    }

    /**
     * method that resets the climber encoders to 0 
     */
    public void resetClimbers(){
        straightClimber0.getEncoder().setPosition(0); 
        straightClimber1.getEncoder().setPosition(0);
    }

}
