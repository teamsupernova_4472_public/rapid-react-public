package frc.robot.subsystems;

import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel.MotorType;

import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.SubsystemBase;
import frc.robot.Constants;

public class BallIntakeFlipOut extends SubsystemBase {
    private final CANSparkMax intakeFlipOut;

    public BallIntakeFlipOut()
    {
        intakeFlipOut = new CANSparkMax(Constants.INTAKE_FLIPOUT_PORT, MotorType.kBrushless);
        intakeFlipOut.getEncoder().setPosition(0);
        SubsystemUtils.setCurrentLimits(intakeFlipOut, 15);
    }

    public void moveVoltage(double volts)
    {
        SmartDashboard.putNumber("Flip out volts", volts);
        intakeFlipOut.setVoltage(volts);
    }

    public void moveSpeed(double speed)
    {
        intakeFlipOut.set(speed);
    }

    public void flipOrRetractIntake(boolean flipOut) {
        double position = getPosition();
        if(flipOut && position < Constants.DEPLOY_INTAKE_POS) {
            intakeFlipOut.setVoltage(Constants.DEPLOY_INTAKE_VOLTS);
        } else if(!flipOut && position > Constants.RETRACT_INTAKE_POS) {
            intakeFlipOut.setVoltage(-Constants.DEPLOY_INTAKE_VOLTS);
        } else {
            intakeFlipOut.set(0.0);
        }
    }

    public void flipOrRetractIntakeOverride(boolean flipOut, boolean override) {
        if(override) {
            intakeFlipOut.setVoltage(-Constants.DEPLOY_INTAKE_VOLTS);
            intakeFlipOut.getEncoder().setPosition(0);
        } else {
            flipOrRetractIntake(flipOut);
        }
    } 

    public double getPosition() {
        return intakeFlipOut.getEncoder().getPosition();
    }

    public double getVoltage() {
        return intakeFlipOut.getBusVoltage() * intakeFlipOut.getAppliedOutput();
    }

    @Override
    public void periodic() {
        double position = getPosition();
        SmartDashboard.putNumber("Position", position);
    }

}
