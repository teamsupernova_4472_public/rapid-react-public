package frc.robot.commands;


import edu.wpi.first.wpilibj.XboxController;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.BallIntake;

public class BallIntakeTeleOp extends CommandBase
{
    private final BallIntake ballIntake; //  ballIntake object
    private final XboxController controller; // controller object
    private final XboxController pController;

    public BallIntakeTeleOp(BallIntake bIntake, XboxController xController, XboxController pCon) // constructor
    {
        ballIntake = bIntake;
        controller = xController;
        pController = pCon;
        addRequirements(ballIntake);
    }

    @Override
    public void execute() // exceutes the ball intake
    {
        boolean intakeReverse = controller.getLeftTriggerAxis() > 0; //|| pController.getRightTriggerAxis() > 0; 
        boolean intakeForward = controller.getRightTriggerAxis() > 0;// || pController.getRightBumper();

        if(intakeReverse)
        {
            ballIntake.setIntakeVolts(Constants.DEFAULT_INTAKE_VOLTS);
        }
        else if(intakeForward)
        {
            ballIntake.setIntakeVolts(-Constants.DEFAULT_INTAKE_VOLTS);
        }
        else
        {
            ballIntake.setIntakeSpeed(0); 
        }   
    }
}
