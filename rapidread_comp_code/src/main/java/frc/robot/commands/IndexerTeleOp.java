package frc.robot.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import frc.robot.Constants;
import frc.robot.subsystems.Indexer;

public class IndexerTeleOp extends CommandBase
{
    private final Indexer index; // new Indexer object
    private final XboxController dController; // Controller object
    private final XboxController pController;
    private double flywheelSpeed;
    private double targetSpeed;

    public IndexerTeleOp(Indexer ix, XboxController xC, XboxController pC) /// Constructor
    {
        index = ix;
        dController = xC;
        pController = pC;
        addRequirements(ix);
    }

    @Override
    public void initialize()
    {} // nothing to deploy, so nothing in initialize method

    @Override
    public void execute() 
    {
        boolean indexUp = dController.getRightBumper(); // converyor belt taking balls up, uses right bumper
        boolean indexDownWithUltra = dController.getRightTriggerAxis() > 0;// || pController.getRightBumper() || pController.getBButton();
        boolean indexDown = dController.getBButton();//  || dController.getRightTriggerAxis() > 0; // conveyor belt taking balls down, uses left bumper
        boolean overrideUp = false; // pController.getAButton(); //Change this button

        targetSpeed = SmartDashboard.getNumber("Target Flywheel speed", Constants.MAX_FLY_WHEEL_SPEED);
        flywheelSpeed = SmartDashboard.getNumber("Current Flywheel Speed", 0);
        SmartDashboard.putBoolean("Flywheel Can Shoot", flywheelSpeed >= targetSpeed && targetSpeed > 0); //TEMPORARY
        
        if(overrideUp)
        {
            //index.indexerMoveVolts(Constants.DEFAULT_INDEXER_VOLTS); // moves at 1.0 speed
        }
        else if(indexUp && flywheelSpeed >= targetSpeed && targetSpeed > 0) // if indexUp is true
            index.indexerMoveVolts(Constants.DEFAULT_INDEXER_VOLTS); // moves at 1.0 speed
        else if(indexDown) // indexDown is true
            index.indexerMoveVolts(-Constants.DEFAULT_INDEXER_VOLTS); // motor moves in reverse speed
        else if(indexDownWithUltra) {
            index.indexerDownWithUltra();
        }
        else index.indexerMove(0); // or doesn't move at all

    }
}
