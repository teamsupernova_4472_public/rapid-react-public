package frc.robot.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallIntakeFlipOut;

public class BallIntakeFlipOutTeleOp extends CommandBase{

    private XboxController mController;
    private XboxController pController;
    private BallIntakeFlipOut ballIntakeFlip;
    private boolean intakeDeploy;
    private final double threshold = 0.5;

    public BallIntakeFlipOutTeleOp(BallIntakeFlipOut flip, XboxController mCon, XboxController pCon)
    {
        ballIntakeFlip = flip;
        mController = mCon;
        pController = pCon;
        addRequirements(ballIntakeFlip);
    }

    @Override
    public void initialize()
    {
        intakeDeploy = true;
    }

    @Override
    public void execute(){
        boolean intakeDeploy = mController.getLeftTriggerAxis() > threshold || mController.getRightTriggerAxis() > threshold;// || pController.getRightTriggerAxis() > 0 || pController.getRightBumper();
        ballIntakeFlip.flipOrRetractIntakeOverride(intakeDeploy, mController.getLeftBumper());
    }
    
}
