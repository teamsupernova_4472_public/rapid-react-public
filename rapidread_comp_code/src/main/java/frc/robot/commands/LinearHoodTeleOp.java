package frc.robot.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LinearHood;

public class LinearHoodTeleOp extends CommandBase
{
    private final LinearHood hood;
    private XboxController controller;
    private boolean state;

    public LinearHoodTeleOp(LinearHood h, XboxController controller) /// Constructor
    {
        hood = h;
        this.controller = controller;
        addRequirements(h);
    }

    @Override 
    public void initialize()
    {
        state = false;
    }

    @Override
    public void execute() 
    {
        if(controller.getLeftBumperPressed())
            state = !state;
        
        // if(state)
        //     hood.setHoodState(Value.kForward);
        // else
            hood.setHoodState(Value.kReverse);

        // if(controller.getAButtonPressed())
        // {
        //     curPos = (curPos == Constants.HIGH_ACTUATOR_POSITION) ? Constants.LOW_ACTUATOR_POSITION : Constants.HIGH_ACTUATOR_POSITION;
        // }   
    }
}
