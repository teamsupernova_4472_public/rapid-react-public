package frc.robot.commands.Autopaths;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.Constants;
import frc.robot.commands.DriveStraight;
import frc.robot.commands.DriveTrainMotionProfileCommand;
import frc.robot.commands.Turn;
import frc.robot.subsystems.DriveTrain;

public class ExampleAutoPath extends SequentialCommandGroup {
    private PIDController lController = new PIDController(Constants.DRIVE_P, Constants.DRIVE_I, Constants.DRIVE_D);
    private PIDController rController = new PIDController(Constants.DRIVE_P, Constants.DRIVE_I, Constants.DRIVE_D);

    public ExampleAutoPath(DriveTrain drive){
        addCommands(
            new DriveTrainMotionProfileCommand("outputs/four_ball_low.wpilib.json",drive)
            );
    }
}
