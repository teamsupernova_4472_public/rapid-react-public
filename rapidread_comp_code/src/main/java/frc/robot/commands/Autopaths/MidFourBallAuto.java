package frc.robot.commands.Autopaths;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.*;
import frc.robot.sensors.Limelight;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.BallIntakeFlipOut;
import frc.robot.subsystems.Conveyor;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.LinearHood;

public class MidFourBallAuto extends SequentialCommandGroup
{
    public MidFourBallAuto(DriveTrain drTrain, BallIntake intake, BallIntakeFlipOut intakeFlip, Indexer index, LinearHood hood, 
                            Flywheel flywheel, Conveyor conveyor, Limelight limeLight)
    {



    addCommands(
            new FlywheelAuton(Constants.MID_TWO_BALL_FLYWHEEL_SPEED, flywheel),
            new ParallelCommandGroup(
                new BallIntakeFlipOutAuton(intakeFlip, true, Integer.MAX_VALUE), // intake flips out
                new SequentialCommandGroup(
                    new WaitCommand(Constants.INTAKE_DEPLOY_TIME_SECONDS), // wait for intake to flip out
                    new ParallelCommandGroup(
                        new BallIntakeAuton(intake, -Constants.DEFAULT_INTAKE_VOLTS, Integer.MAX_VALUE), // intake runs
                        new SequentialCommandGroup(
                            new DriveStraight(drTrain, 60, 0.5, 1500), // drives back and intakes ball
                            new ParallelCommandGroup(
                                new LimelightTurn(drTrain, limeLight, 1000),
                                new LimelightRPM(flywheel, limeLight, 1000)
                            ),
                            new ParallelCommandGroup(
                                new ConveyorAuton(-Constants.DEFAULT_CONVEYOR_VOLTS, conveyor, 2500), // conveyor 
                                new IndexerAuton(index, Constants.DEFAULT_INDEXER_VOLTS, 2500) // and indexer runs
                            ),
                            new FlywheelAuton(0, flywheel), // shoots ball
                            //new Turn(drTrain, 90, 500),
                            new DriveTrainMotionProfileCommand("paths/four_ball_mid.wpilib.json",drTrain), // motion profiling starts
                            
                            new ParallelCommandGroup(
                                new SequentialCommandGroup(
                                    new WaitCommand(1.0), 
                                    new DriveStraight(drTrain, -100, 0.5, 1500),
                                    //new FlywheelAuton(Constants.FOUR_BALL_FLYWHEEL_SPEED, flywheel),
                                    new ParallelCommandGroup(
                                        new LimelightTurn(drTrain, limeLight, 1250),
                                        new LimelightRPM(flywheel, limeLight, 1250)
                                    )
                                    
                                ),
                                new SequentialCommandGroup(
                                    new UltraSonicIndex(index, conveyor, 4000), // keeps indexer and converyor running throughout
                                    new ParallelCommandGroup(
                                        new ConveyorAuton(-Constants.DEFAULT_CONVEYOR_VOLTS, conveyor, Integer.MAX_VALUE),
                                        new IndexerAuton(index, Constants.DEFAULT_INDEXER_VOLTS, Integer.MAX_VALUE)
                                    )
                                )

                            ),
                            
                            new WaitCommand(Double.MAX_VALUE)
                        )
                    )
                )
            )
        );

    }
}   
