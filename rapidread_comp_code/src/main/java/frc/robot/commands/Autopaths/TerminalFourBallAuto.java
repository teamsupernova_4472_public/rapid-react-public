package frc.robot.commands.Autopaths;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.ParallelRaceGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.*;
import frc.robot.sensors.Limelight;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.BallIntakeFlipOut;
import frc.robot.subsystems.Conveyor;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.LinearHood;


public class TerminalFourBallAuto extends SequentialCommandGroup
{ 
    public TerminalFourBallAuto(DriveTrain drTrain, BallIntake intake, BallIntakeFlipOut intakeFlip, Indexer index, LinearHood hood, 
            Flywheel flywheel, Conveyor conveyor, Limelight limeLight)
    {

        addCommands(
            new FlywheelAuton(Constants.TWO_BALL_FLYWHEEL_SPEED, flywheel),
            new ParallelCommandGroup(
                new BallIntakeFlipOutAuton(intakeFlip, true, Integer.MAX_VALUE),
                new SequentialCommandGroup(
                    new WaitCommand(Constants.INTAKE_DEPLOY_TIME_SECONDS),
                    new ParallelCommandGroup(
                        new BallIntakeAuton(intake, -Constants.DEFAULT_INTAKE_VOLTS, Integer.MAX_VALUE),
                        new SequentialCommandGroup(
                            new DriveStraight(drTrain, 43, 0.5, 700),
                            new ParallelCommandGroup(
                                new LimelightTurn(drTrain, limeLight, 1000),
                                new LimelightRPM(flywheel, limeLight, 1000)
                            ),
                            new ParallelCommandGroup(
                                new ConveyorAuton(-Constants.DEFAULT_CONVEYOR_VOLTS, conveyor, 2500),
                                new IndexerAuton(index, Constants.DEFAULT_INDEXER_VOLTS, 2500)
                            ),
                            new FlywheelAuton(0, flywheel),
                            new Turn(drTrain, 90, 500),
                            new DriveTrainMotionProfileCommand("paths/four_ball_top.wpilib.json",drTrain),
                            
                            new ParallelCommandGroup(
                                new SequentialCommandGroup(
                                    new WaitCommand(0.5),
                                    new DriveStraight(drTrain, -100, 0.5, 1500),
                                    new Turn(drTrain, 90, 1000),
                                    new ParallelCommandGroup(
                                        new LimelightTurn(drTrain, limeLight, 1000),
                                        new LimelightRPM(flywheel, limeLight, 1000)
                                    )
                                ),
                                new SequentialCommandGroup(
                                    new UltraSonicIndex(index, conveyor, 1500),
                                    new WaitCommand(2.5),
                                    new ParallelCommandGroup(
                                        new ConveyorAuton(-Constants.DEFAULT_CONVEYOR_VOLTS, conveyor, Integer.MAX_VALUE),
                                        new IndexerAuton(index, Constants.DEFAULT_INDEXER_VOLTS, Integer.MAX_VALUE)
                                    )
                                )

                            ),
                            
                            new WaitCommand(Double.MAX_VALUE)
                        )
                    )
                )
            )
        );

    }

    
    
}
