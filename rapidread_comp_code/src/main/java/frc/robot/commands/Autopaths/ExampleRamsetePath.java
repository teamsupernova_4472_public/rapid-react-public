package frc.robot.commands.Autopaths;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import frc.robot.subsystems.DriveTrain;

import static frc.robot.Constants.*;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

public class ExampleRamsetePath extends SequentialCommandGroup {

    private final DriveTrain driveTrain;
    private final Trajectory trajectory;
    public ExampleRamsetePath(DriveTrain driveTrain) {
        this.driveTrain = driveTrain;
        // Create a voltage constraint to ensure we don't accelerate too fast
        var autoVoltageConstraint =
            new DifferentialDriveVoltageConstraint(
                new SimpleMotorFeedforward(
                    ksVolts,
                    kvVoltSecondsPerMeter,
                    kaVoltSecondsSquaredPerMeter),
                kDriveKinematics,
                10);
        

        // Create config for trajectory
        TrajectoryConfig config =
            new TrajectoryConfig(
                    kMaxSpeedMetersPerSecond,
                    kMaxAccelerationMetersPerSecondSquared)
                // Add kinematics to ensure max speed is actually obeyed
                .setKinematics(kDriveKinematics)
                // Apply the voltage constraint
                .addConstraint(autoVoltageConstraint);

        // An example trajectory to follow.  All units in meters.
        String trajectoryJSON = "paths/four_ball.wpilib.json";
        Trajectory trajectoryToSet = TrajectoryGenerator.generateTrajectory(
            // Start at the origin facing the +X direction
            new Pose2d(0, 0, new Rotation2d(0)),
            // Pass through these two interior waypoints, making an 's' curve path
            List.of(
                //new Translation2d(1, 1), new Translation2d(2, -1)
            ),
            // End 3 meters straight ahead of where we started, facing forward
            new Pose2d(3, 0, new Rotation2d(0)),
            // Pass config
            config);
        try {
            Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(trajectoryJSON);
            trajectoryToSet = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
        } catch (IOException ex) {
            DriverStation.reportError("Unable to open trajectory: " + trajectoryJSON, ex.getStackTrace());
        }
        trajectory = trajectoryToSet;

        RamseteCommand ramseteCommand =
            new RamseteCommand(
                trajectory,
                driveTrain::getPose,
                new RamseteController(kRamseteB, kRamseteZeta),
                new SimpleMotorFeedforward(
                    ksVolts,
                    kvVoltSecondsPerMeter,
                    kaVoltSecondsSquaredPerMeter),
                kDriveKinematics,
                driveTrain::getWheelSpeeds,
                new PIDController(kPDriveVel, 0, 0),
                new PIDController(kPDriveVel, 0, 0),
                // RamseteCommand passes volts to the callback
                driveTrain::driveTankVolts,
                driveTrain);

        addCommands(ramseteCommand);
    }

    @Override
    public void initialize() {
        super.initialize();
         // Reset odometry to the starting pose of the trajectory.
         driveTrain.resetOdometry(trajectory.getInitialPose());
    }
    
}
