package frc.robot.commands.Autopaths;

import edu.wpi.first.wpilibj2.command.ParallelCommandGroup;
import edu.wpi.first.wpilibj2.command.SequentialCommandGroup;
import edu.wpi.first.wpilibj2.command.WaitCommand;
import frc.robot.Constants;
import frc.robot.commands.*;
import frc.robot.sensors.Limelight;
import frc.robot.subsystems.BallIntake;
import frc.robot.subsystems.BallIntakeFlipOut;
import frc.robot.subsystems.Conveyor;
import frc.robot.subsystems.DriveTrain;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.subsystems.LinearHood;

public class MidTwoBallAuto extends SequentialCommandGroup
{
    public MidTwoBallAuto(DriveTrain drTrain, BallIntake intake, BallIntakeFlipOut intakeFlip, Indexer index, LinearHood hood, 
            Flywheel flywheel, Conveyor conveyor, Limelight limeLight)
    {
        addCommands(
           new FlywheelAuton(Constants.MID_TWO_BALL_FLYWHEEL_SPEED, flywheel),
           new ParallelCommandGroup(
                new BallIntakeFlipOutAuton(intakeFlip, true, Integer.MAX_VALUE),
                new SequentialCommandGroup(
                    new WaitCommand(Constants.INTAKE_DEPLOY_TIME_SECONDS),
                    new ParallelCommandGroup(
                        new BallIntakeAuton(intake, -Constants.DEFAULT_INTAKE_VOLTS, Integer.MAX_VALUE),
                        new SequentialCommandGroup(
                            new DriveStraight(drTrain, 60, 0.5, 2000),
                            new LimelightTurn(drTrain, limeLight, 2500),
                            new ParallelCommandGroup(
                                new ConveyorAuton(-Constants.DEFAULT_CONVEYOR_VOLTS, conveyor, Integer.MAX_VALUE),                
                                new IndexerAuton(index, Constants.DEFAULT_INDEXER_VOLTS, Integer.MAX_VALUE)
                            )
                        )
                    )
                )
           ),
           new WaitCommand(Double.MAX_VALUE)
        );
    }

    
}
