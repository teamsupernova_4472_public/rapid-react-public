package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Conveyor;

public class ConveyorAuton extends CommandBase{
    double voltage;
    int timeout;
    Timer timer = new Timer();
    Conveyor conveyor;

    public ConveyorAuton(double v, Conveyor c, int t)
    {
        conveyor = c;
        voltage = v;
        timeout = t;
        addRequirements(c);
    }

    @Override
    public void initialize(){
     timer.reset();
     timer.start();
    }

    @Override
    public void execute(){
        conveyor.setVolts(voltage);
    }

    @Override
    public void end(boolean interrupted){
        timer.stop();
        conveyor.setSpeed(0);
    }

    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }


}
