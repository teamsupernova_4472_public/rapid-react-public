package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Climber;

public class ClimbTeleOp extends CommandBase {

    private final Climber m_climber;
    private final XboxController m_controller;
    /**
     * constructor for the manual climb
     * @param climber
     * @param controller
     */
    public ClimbTeleOp(Climber climber, XboxController controller){
        m_climber = climber;
        m_controller = controller;
        addRequirements(climber);
    }
    @Override
    public void initialize() {
       m_climber.resetClimbers();   
    }
  
    // Called every time the scheduler runs while the command is scheduled.
    /**
     * sets the straight and angle climbers to the value pulled from the controller 
     */
    @Override
    public void execute() {
      //Reset encoders
      if(m_controller.getRightBumper())
      {
        m_climber.resetClimbers();
      }

      //Angle climb activate
      if(m_controller.getLeftTriggerAxis() > 0)
        m_climber.angleClimbers(Value.kForward);
      else
        m_climber.angleClimbers(Value.kReverse);
      
      //Controller math
      double straightVal = MathUtil.applyDeadband(m_controller.getLeftY(), 0.1);
      /*if(straightVal > 0)
      {
        m_climber.straightClimb(-straightVal);
        return;
      }*/

      double leftPos = m_climber.getLeftClimbPos();
      double rightPos = m_climber.getRightClimbPos();

      //Bounds checking
      if(leftPos > -Constants.CLIMBER_LEFT_UPPER_LIMIT && straightVal < -0.1)
      {
        m_climber.straightLeftClimb(-straightVal);
      }
      else if(leftPos <= 0.0 && straightVal > 0.1)
      {
        m_climber.straightLeftClimb(-straightVal);
      }
      else
      {
        m_climber.straightLeftClimb(0);
      }

      if(rightPos < Constants.CLIMBER_RIGHT_UPPER_LIMIT && straightVal < -0.1)
      {
        m_climber.straightRightClimb(-straightVal);
      }
      else if(rightPos >= 0.0 && straightVal > 0.1)
      {
        m_climber.straightRightClimb(-straightVal);
      }
      else
      {
        m_climber.straightRightClimb(0);
      }

      if(straightVal == 0.0)
      {
        m_climber.straightClimb(-MathUtil.applyDeadband(m_controller.getRightY(), 0.1));
      }
    }
  
    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}
  
    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
      return false;
    }
}
