package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.LinearHood;

public class LinearHoodAuton extends CommandBase {
    //varibles and classes needed for the class
    LinearHood hood;
    Value pos;
    Timer timer = new Timer();
    int timeout;

    //constructor for the IndexerAutonCommand class
    public LinearHoodAuton(LinearHood hood, Value p, int t){
        this.hood = hood;
        pos = p;
        timeout = t;
        addRequirements(hood);
    }

    public LinearHoodAuton(LinearHood hood, Value p){
        this(hood, p, Integer.MAX_VALUE);
    }

    //Initialization - resets and starts the timer for timeout logic later
    @Override
    public void initialize(){
        timer.reset();
        timer.start();
    }

    //Execute - moves the indexer based on the value of the speed varible
    @Override
    public void execute(){
        hood.setHoodState(pos);
    }

    //End - stops the timer and the indexer
    @Override
    public void end(boolean interrupted){
        timer.stop();
    }

    //isFinished - when the task has timed out, it returns true because the task is finished
    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }
}
