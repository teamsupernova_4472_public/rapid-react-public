package frc.robot.commands;

import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;

public class ResetOdometry extends CommandBase {

    private final DriveTrain dTrain;
    private final Timer timer;
    private final double timeout;
    private final Trajectory trajectory;

    public ResetOdometry(DriveTrain train, Trajectory traj, double timeOut)
    {
        dTrain = train;
        timer = new Timer();
        timeout = timeOut;
        trajectory = traj;
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
        dTrain.resetOdometry(trajectory.getInitialPose());
    }
    
    @Override
    public void end(boolean interrupted){
        timer.stop();
    }

    //isFinished - when the task has timed out, it returns true because the task is finished
    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }
}
