package frc.robot.commands;

import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Conveyor;

public class ConveyorTeleop  extends CommandBase{
    private final XboxController controller;
    private final XboxController partner;
    private final Conveyor conveyor;


    public ConveyorTeleop(XboxController control, XboxController partner, Conveyor con)
    {
        this.partner = partner;
        controller = control;
        conveyor = con;
        addRequirements(con);
    }

    @Override
    public void execute(){
        boolean conveyorUp = controller.getRightBumper();
        boolean conveyorDown = controller.getBButton();
        boolean conveyorUpWithUltra = controller.getRightTriggerAxis() > 0;// || partner.getRightBumper() || partner.getBButton();

        if(conveyorDown)
            conveyor.setVolts(Constants.DEFAULT_CONVEYOR_VOLTS);
        else if(conveyorUp) {
            conveyor.setVolts(-Constants.DEFAULT_CONVEYOR_VOLTS);
        }
        else if(conveyorUpWithUltra)
        {
            conveyor.conveyorUpWithUltra();
        }
        else
            conveyor.setSpeed(0.0);
    }
}
