package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.Constants;
import frc.robot.subsystems.Conveyor;
import frc.robot.subsystems.Flywheel;
import frc.robot.subsystems.Indexer;
import frc.robot.sensors.*;

public class FlywheelTeleOp extends CommandBase
{
    private final Flywheel flyWheel;
    private final XboxController controllerD;
    private final XboxController controllerP;
    private double speed;
    private final Limelight limelight;
    private boolean enabledPartner = false;

    public double findVelocity() {
        double area = limelight.getYDegreesAwayFromTarget();
        speed = Constants.QUART_A*Math.pow(area, 4) + Constants.QUART_B*Math.pow(area, 3) + Constants.QUART_C*Math.pow(area, 2) 
                + Constants.QUART_D*Math.pow(area, 1) + Constants.QUART_E;
        SmartDashboard.putNumber("Y deg", area);
        SmartDashboard.putNumber("Calculated FlyWheel Speed", speed);
        return speed;
    }

    public FlywheelTeleOp(Flywheel flyWheel, double speed, XboxController controllerD, XboxController controllerP, Limelight limelight){
        this.flyWheel = flyWheel;
        this.speed = speed;
        this.limelight = limelight;
        this.controllerD = controllerD;
        this.controllerP = controllerP;
        addRequirements(flyWheel);
    }

    public double getTargetSpeed()
    {
        return speed;
    }

    @Override
    public void end(boolean interrputed) {
        flyWheel.setSpeed(0);
    }

    @Override
    public void execute()
    {
        boolean pressedPartner = controllerP.getAButtonPressed();  //changed to partner control
        boolean pressedDriver = controllerD.getAButton();
        if(pressedPartner) 
        {
            enabledPartner = !enabledPartner;
        }
        if(controllerD.getXButton()) 
        {
            flyWheel.setSpeed(-0.1);
        }
        else if (pressedDriver) 
        {
            double speed = findVelocity();
            flyWheel.setSpeed(speed);
        } 
        else if(!enabledPartner && controllerD.getRightBumper()) 
        {
            flyWheel.setSpeed(Constants.MAX_FLY_WHEEL_SPEED *0.3);
        }
        else if(enabledPartner)
        {
            if(controllerP.getXButtonPressed()) 
            {
                speed -= 0.01 * Constants.MAX_FLY_WHEEL_SPEED;
                speed = MathUtil.clamp(speed, 0.0, Constants.MAX_FLY_WHEEL_SPEED);
            }
    
            if(controllerP.getYButtonPressed()) 
            {
                speed += 0.01 * Constants.MAX_FLY_WHEEL_SPEED;
                speed = MathUtil.clamp(speed, 0.0, Constants.MAX_FLY_WHEEL_SPEED);
            }

            flyWheel.setSpeed(speed);
        }
        else 
        {
            flyWheel.setPercentage(0.0);
        }

        SmartDashboard.putNumber("Percent output", speed/Constants.MAX_FLY_WHEEL_SPEED);
    }

    @Override
    public boolean isFinished() {
        return false;
    }
}
