package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallIntakeFlipOut;

public class BallIntakeFlipOutTeleOpTester extends CommandBase
{
    private final BallIntakeFlipOut ballIntakeFlipOut;
    private final XboxController driver;
    private final XboxController partner;

    boolean flipOut = false;

    public BallIntakeFlipOutTeleOpTester(BallIntakeFlipOut flipOut, XboxController driverControl, 
        XboxController partnerControl) {
        ballIntakeFlipOut = flipOut;
        driver = driverControl;
        partner = partnerControl;
        addRequirements(flipOut);
    }

    @Override
    public void execute() {
        boolean pressed = driver.getLeftBumperPressed() || partner.getRightBumperPressed();
        double desiredVolts = 3*MathUtil.applyDeadband(partner.getRightY(), 0.1);
        ballIntakeFlipOut.moveVoltage(desiredVolts);
        SmartDashboard.putNumber("flipout encoder", ballIntakeFlipOut.getPosition());
        SmartDashboard.putNumber("flipout volts", desiredVolts);
        // if(pressed) {
        //     flipOut = !flipOut;
        // }

        // if(flipOut) {
        //     ballIntakeFlipOut.setVoltage(1.0);
        // } else {
        //     ballIntakeFlipOut.setVoltage(-1.0);
        // }
    }
  
    // Called once the command ends or is interrupted.
    @Override
    public void end(boolean interrupted) {}
  
    // Returns true when the command should end.
    @Override
    public boolean isFinished() {
        return false;
    }
}
