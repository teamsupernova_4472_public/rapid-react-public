package frc.robot.commands;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import static frc.robot.Constants.*;
import frc.robot.subsystems.DriveTrain;


public class Turn extends CommandBase 
{
    PIDController gyroPID = new PIDController(GYRO_P, GYRO_I, GYRO_D);
    Timer timer = new Timer();
    final int timeout;

    
    DriveTrain drivetrain;
    double heading;

    public Turn(DriveTrain pDriveTrain, double pHeading, int time)
    {
        drivetrain = pDriveTrain;
        heading = pHeading;
        timeout = time;
        addRequirements(pDriveTrain);
    }

    @Override
    public void initialize()
    {
        timer.reset();
        timer.start();

    }

    @Override
    public void execute()
    {
        double angle = drivetrain.gyro.getAngle();
        double output = gyroPID.calculate(angle, heading);
        drivetrain.driveArcade(0,output);

    }

    @Override 
    public void end(boolean interrupted){
        timer.stop();
        drivetrain.turn(0);
    }

    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }




}
