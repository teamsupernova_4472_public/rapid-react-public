package frc.robot.commands;


import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallIntake;

public class BallIntakeAuton extends CommandBase {
    BallIntake intake;
    Timer timer = new Timer();
    final int timeout;
    double voltage;

    public BallIntakeAuton(BallIntake in, double volt, int time){
        intake = in;
        timeout = time;
        voltage = volt;
        addRequirements(in);
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
    }

    @Override
    public void execute(){
        intake.setIntakeVolts(voltage);
    }

    @Override
    public void end(boolean interrupted){
        timer.stop();
        intake.setIntakeSpeed(0);
    }

    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }
}
