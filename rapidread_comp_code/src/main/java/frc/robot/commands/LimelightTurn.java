package frc.robot.commands;

import com.ctre.phoenix.motorcontrol.ControlMode;

import edu.wpi.first.networktables.NetworkTableEntry;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj2.command.PIDCommand;
import edu.wpi.first.math.MathUtil;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;
import frc.robot.sensors.Limelight;

public class LimelightTurn extends PIDCommand{
    private final Timer timer = new Timer();
    private final int timeout;
    private final PIDController controller;
    private final NetworkTableEntry pEntry;
    private final NetworkTableEntry iEntry;
    private final NetworkTableEntry dEntry;
    private final DriveTrain driveTrain;
    public double limeTurnValue;
    private final Limelight limeLight;

    public LimelightTurn(DriveTrain driveTrain, Limelight limeLight, int timeout) {
        this(driveTrain, limeLight, timeout, null, null, null);
    }

    
    public LimelightTurn(DriveTrain driveTrain, Limelight limeLight, int timeout,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        this(driveTrain, limeLight, timeout, new PIDController(Constants.LIMELIGHT_P, Constants.LIMELIGHT_I, Constants.LIMELIGHT_D), 
                pEntry, iEntry, dEntry);
    }

    private LimelightTurn(DriveTrain driveTrain, Limelight limeLight, int timeout, PIDController controller,
                NetworkTableEntry pEntry, NetworkTableEntry iEntry, NetworkTableEntry dEntry) {
        super(controller, //<-- PID Controller
            limeLight::getXDegreesAwayFromTarget, // <-- access to gyro angle method
            0, //angle you want to go to
            output -> {
                double clamped = MathUtil.clamp(output, -1, 1);
                driveTrain.leftDrive.set(ControlMode.PercentOutput,  clamped);
                driveTrain.rightDrive.set(ControlMode.PercentOutput, clamped);
            }, //<--output command on robot
            driveTrain, //drive train requirement
            limeLight//sensor requirement
            );
        this.timeout = timeout;
        this.controller = controller;
        this.pEntry = pEntry;
        this.iEntry = iEntry;
        this.dEntry = dEntry;
        this.driveTrain = driveTrain;
        limeTurnValue = 0;
        this.limeLight = limeLight;
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
        controller.reset();
        if(pEntry != null && iEntry != null && dEntry != null) {
            controller.setP(pEntry.getDouble(0));
            controller.setI(iEntry.getDouble(0));
            controller.setD(dEntry.getDouble(0));
        }
        //limeLight.setLedMode(0);
    }

    @Override
    public void end(boolean interrupted) {
        timer.stop();
        driveTrain.turn(0);
        if(interrupted) {
            //limeLight.setLedMode(1);
        }
    }

    @Override
    public boolean isFinished() {
        boolean isFinished = timeout < timer.get() * 1000;
        return isFinished;
    }
     
}
