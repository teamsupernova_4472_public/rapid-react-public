package frc.robot.commands;

import edu.wpi.first.math.MathUtil;
import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.DriveTrain;
import frc.robot.Constants;

public class DriveStraight extends CommandBase{

    PIDController drivePID = new PIDController(Constants.DRIVE_P, Constants.DRIVE_I, Constants.DRIVE_D);
    PIDController gyroPID = new PIDController(Constants.GYRO_P, Constants.GYRO_I, Constants.GYRO_D);
    double maxSpeed;
    Timer timer = new Timer();
    final int timeout;
    DriveTrain drivetrain;
    double distance;
    double targetPos;
    double targetAngle;
    

    public DriveStraight(DriveTrain drive, double dist, double max, int time){
        drivetrain = drive;
        distance = dist*Constants.ENCODER_TICKS_PER_INCH;
        timeout = time;
        maxSpeed = max;
        addRequirements(drive);
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
        double leftEncoder = -drivetrain.leftDrive.getSensorCollection().getIntegratedSensorPosition();
        double rightEncoder = drivetrain.rightDrive.getSensorCollection().getIntegratedSensorPosition();
        targetPos = ((leftEncoder + rightEncoder)/2) + distance;
        targetAngle = drivetrain.gyro.getAngle();
    }
    
    @Override
    public void execute(){
        double leftEncoder = -drivetrain.leftDrive.getSensorCollection().getIntegratedSensorPosition();
        double rightEncoder = drivetrain.rightDrive.getSensorCollection().getIntegratedSensorPosition();
        double averageDistance = (leftEncoder + rightEncoder)/2;
        double output = MathUtil.clamp(drivePID.calculate(averageDistance, targetPos), -maxSpeed, maxSpeed);
        double gyroOutput = MathUtil.clamp(gyroPID.calculate(drivetrain.gyro.getAngle(), targetAngle), -maxSpeed, maxSpeed);
        drivetrain.driveArcade(output, gyroOutput);
        SmartDashboard.putNumber("PID output", output);
        SmartDashboard.putNumber("Avg dist", averageDistance);
        SmartDashboard.putNumber("Target dist", targetPos);
    }

    @Override
    public void end(boolean interrupted){
        timer.stop();
        drivetrain.drive(0);
    }

    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }
}
