package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.Ultrasonic;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Conveyor;
import frc.robot.subsystems.Indexer;

public class UltraSonicIndex extends CommandBase {
    private final Indexer indexer;
    private final Conveyor conveyor;
    Timer timer = new Timer();
    final int timeout;

    public UltraSonicIndex(Indexer i, Conveyor c, int timeOut)
    {
        indexer = i;
        conveyor = c;
        timeout = timeOut;
        addRequirements(indexer, conveyor);
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
    }

    @Override
    public void execute(){
        indexer.indexerDownWithUltra();
        conveyor.conveyorUpWithUltra();
    }

    @Override
    public void end(boolean interrupted){
        timer.stop();
        indexer.indexerMoveVolts(0);
        conveyor.setVolts(0);
    }

    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }

    
}
