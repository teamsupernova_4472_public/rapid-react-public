package frc.robot.commands;

import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Flywheel;

public class FlywheelAuton extends CommandBase
{
    private final Flywheel flywheel;
    double speed; 
    public FlywheelAuton(double s, Flywheel f){
        this.speed = s;
        this.flywheel = f;
        addRequirements(f);
    }

    @Override
    public void initialize(){
        flywheel.setSpeed(speed);
    }
    
    @Override
    public boolean isFinished(){
        return true;
    }

}