package frc.robot.commands;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

import edu.wpi.first.math.controller.PIDController;
import edu.wpi.first.math.controller.RamseteController;
import edu.wpi.first.math.controller.SimpleMotorFeedforward;
import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.geometry.Rotation2d;
import edu.wpi.first.math.geometry.Translation2d;
import edu.wpi.first.math.trajectory.Trajectory;
import edu.wpi.first.math.trajectory.TrajectoryConfig;
import edu.wpi.first.math.trajectory.TrajectoryGenerator;
import edu.wpi.first.math.trajectory.TrajectoryUtil;
import edu.wpi.first.math.trajectory.Trajectory.State;
import edu.wpi.first.math.trajectory.constraint.DifferentialDriveVoltageConstraint;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.Filesystem;
import edu.wpi.first.wpilibj2.command.RamseteCommand;
import frc.robot.Constants;
import frc.robot.subsystems.DriveTrain;

public class DriveTrainMotionProfileCommand extends RamseteCommand{
    
    private final static DifferentialDriveVoltageConstraint AUTO_VOLTAGE_CONSTRAINT = 
        new DifferentialDriveVoltageConstraint(
            new SimpleMotorFeedforward(Constants.ksVolts, Constants.kvVoltSecondsPerMeter, Constants.kaVoltSecondsSquaredPerMeter), 
            Constants.kDriveKinematics, 
            10);

    private final static TrajectoryConfig CONFIG = 
        new TrajectoryConfig(Constants.kMaxSpeedMetersPerSecond, Constants.kMaxAccelerationMetersPerSecondSquared)
            .setKinematics(Constants.kDriveKinematics)
            .addConstraint(AUTO_VOLTAGE_CONSTRAINT);

    private final static PIDController RAMSETE_PID = new PIDController(Constants.kPDriveVel, 0, 0);
    private final DriveTrain driveTrain;
    private final Trajectory trajectory;


    public DriveTrainMotionProfileCommand(Pose2d start, List<Translation2d> p, Pose2d end, DriveTrain drive)
    {
        this(createTrajectory(start, p, end, CONFIG), drive);
    }

    public DriveTrainMotionProfileCommand(String pathWeaverFile, DriveTrain drive)
    {
        this(pathWeaverTrajectory(pathWeaverFile), drive);
    }

    public DriveTrainMotionProfileCommand(Trajectory trajectory, DriveTrain drive) {
        super(trajectory, 
        drive::getPose, 
        createController(), 
        new SimpleMotorFeedforward(Constants.ksVolts, Constants.kvVoltSecondsPerMeter, Constants.kaVoltSecondsSquaredPerMeter), 
        Constants.kDriveKinematics, 
        drive::getWheelSpeeds, 
        RAMSETE_PID, 
        RAMSETE_PID, 
        drive::driveTankVolts, 
        drive);
        this.trajectory = trajectory;
        driveTrain = drive;
    }

    private static RamseteController createController() {
        RamseteController controller = new RamseteController();
        return controller;
    }
    

    private static Trajectory createTrajectory(Pose2d startPoint, List<Translation2d> points, Pose2d endPoint, TrajectoryConfig config)
    {
        return TrajectoryGenerator.generateTrajectory(
            startPoint,
            points,
            endPoint,
            config
        );

    }

    private static Trajectory pathWeaverTrajectory(String file)
    {
        // Empty trajectory as default
        Trajectory trajectoryToReturn = createTrajectory(
            new Pose2d(0, 0, new Rotation2d(0)),
            List.of(),
            new Pose2d(1, 0, new Rotation2d(0)),
            CONFIG);
        try {
            Path trajectoryPath = Filesystem.getDeployDirectory().toPath().resolve(file);
            Trajectory parsedTrajectory = TrajectoryUtil.fromPathweaverJson(trajectoryPath);
            List<State> states = parsedTrajectory.getStates();
            if(states != null && states.size() > 1) {
                State startState = states.get(0);
                State endState = states.get(states.size() - 1);
                Pose2d start = new Pose2d(startState.poseMeters.getX(), startState.poseMeters.getY(), startState.poseMeters.getRotation());
                Pose2d end = new Pose2d(endState.poseMeters.getX(), endState.poseMeters.getY(), endState.poseMeters.getRotation());
                List<Translation2d> translation2ds = new ArrayList<>();
                for(int i = 1; i < states.size() - 1; i++) {
                    State currentState = states.get(i);
                    Translation2d translation2d = new Translation2d(currentState.poseMeters.getX(), currentState.poseMeters.getY());
                    translation2ds.add(translation2d);
                }
                trajectoryToReturn = createTrajectory(start, translation2ds, end, CONFIG);
            }
        } catch (IOException ex) {
            DriverStation.reportError("Unable to open trajectory: " + file, ex.getStackTrace());
        }
        return trajectoryToReturn;
    }

    @Override
    public void initialize(){
        driveTrain.resetOdometry(trajectory.getInitialPose());
        super.initialize();  
    }

    @Override
    public void end(boolean interrupted){
        super.end(interrupted);
        driveTrain.driveTankVolts(0.0, 0.0);
    }

}
