package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.BallIntakeFlipOut;

public class BallIntakeFlipOutAuton extends CommandBase
{
    private final BallIntakeFlipOut bInt;
    private final boolean flipOut;
    private final Timer timer = new Timer();
    private final int timeout;

    public BallIntakeFlipOutAuton(BallIntakeFlipOut intakeB, boolean f, int t)
    {
        bInt = intakeB;
        flipOut = f;
        timeout = t;
        addRequirements(intakeB);
    }

    @Override
    public void initialize() {
        timer.reset();
    }

    public void execute() {
        bInt.flipOrRetractIntake(flipOut);
    }

    @Override
    public void end(boolean interrupted){
        timer.stop();
        bInt.moveSpeed(0);
    }

    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }

}
