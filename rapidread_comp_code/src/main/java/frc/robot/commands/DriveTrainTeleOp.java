// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.commands;

import frc.robot.subsystems.DriveTrain;
import edu.wpi.first.math.MathUtil;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj2.command.CommandBase;

/** An DriveTrain command that uses an DriveTrain subsystem. */
public class DriveTrainTeleOp extends CommandBase {
  @SuppressWarnings({"PMD.UnusedPrivateField", "PMD.SingularField"})
  private final DriveTrain m_subsystem;

  private final XboxController controller;

  /**
   * Creates a new DriveTrainCommand.
   *
   * @param subsystem The subsystem used by this command.
   */
  public DriveTrainTeleOp(DriveTrain subsystem, XboxController control) {
    m_subsystem = subsystem;
    controller = control;
    // Use addRequirements() here to declare subsystem dependencies.
    addRequirements(subsystem);
  }

  public void drive(XboxController controller) {
    double leftY = -controller.getLeftY();
    double rightY = -controller.getRightY();
    double rightX = controller.getRightX();
    leftY = MathUtil.applyDeadband(leftY, 0.1);
    rightY = MathUtil.applyDeadband(rightY, 0.1);
    rightX = MathUtil.applyDeadband(rightX, 0.1);
    double rTriggerValue = controller.getRightTriggerAxis();
    rTriggerValue = MathUtil.applyDeadband(rTriggerValue, 0.1);
    if(rTriggerValue > 0)
    {
      leftY *= 0.75;
      rightY *= 0.75;
    }
    m_subsystem.driveTank(leftY, rightY);
    //m_subsystem.driveArcade(leftY, rightX);
  }

  // Called when the command is initially scheduled.
  @Override
  public void initialize() {

  }

  // Called every time the scheduler runs while the command is scheduled.
  @Override
  public void execute() {
    drive(controller);
  }

  // Called once the command ends or is interrupted.
  @Override
  public void end(boolean interrupted) {}

  // Returns true when the command should end.
  @Override
  public boolean isFinished() {
    return false;
  }
}
