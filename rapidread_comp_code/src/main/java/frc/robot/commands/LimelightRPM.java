package frc.robot.commands;


import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Flywheel;
import frc.robot.sensors.Limelight;

public class LimelightRPM extends CommandBase
{
    private final Flywheel flywheel;
    private final Limelight limelight;
    private final Timer timer;
    private double timeout;
    
    public LimelightRPM(Flywheel f, Limelight l, double time){
        this.flywheel = f;
        this.limelight = l;
        timer = new Timer();
        this.timeout = time;
        addRequirements(f);
    }

    @Override
    public void initialize(){
        timer.reset();
        timer.start();
    }

    @Override
    public void execute() {
        double speed = flywheel.findVelocity(limelight);
        flywheel.setSpeed(speed);
    }
    
    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }

}