package frc.robot.commands;

import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj2.command.CommandBase;
import frc.robot.subsystems.Indexer;

public class IndexerAuton extends CommandBase {
    //varibles and classes needed for the class
    Indexer index;
    double speed;
    Timer timer = new Timer();
    int timeout;

    //constructor for the IndexerAutonCommand class
    public IndexerAuton(Indexer i, double s, int t){
        index = i;
        speed = s;
        timeout = t;
        addRequirements(i);
    }

    //Initialization - resets and starts the timer for timeout logic later
    @Override
    public void initialize(){
        timer.reset();
        timer.start();
    }

    //Execute - moves the indexer based on the value of the speed varible
    @Override
    public void execute(){
        index.indexerMoveVolts(speed);
    }

    //End - stops the timer and the indexer
    @Override
    public void end(boolean interrupted){
        timer.stop();
        index.indexerMove(0);
    }

    //isFinished - when the task has timed out, it returns true because the task is finished
    @Override
    public boolean isFinished(){
        boolean isFinished = timeout < timer.get()*1000;
        return isFinished;
    }
}
