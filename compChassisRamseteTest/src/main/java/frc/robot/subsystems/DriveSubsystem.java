// Copyright (c) FIRST and other WPILib contributors.
// Open Source Software; you can modify and/or share it under the terms of
// the WPILib BSD license file in the root directory of this project.

package frc.robot.subsystems;

import com.ctre.phoenix.motorcontrol.can.WPI_TalonFX;
import com.kauailabs.navx.frc.AHRS;

import edu.wpi.first.math.geometry.Pose2d;
import edu.wpi.first.math.kinematics.DifferentialDriveOdometry;
import edu.wpi.first.math.kinematics.DifferentialDriveWheelSpeeds;
import edu.wpi.first.wpilibj.Encoder;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;
import frc.robot.Constants.DriveConstants;
import edu.wpi.first.wpilibj.interfaces.Gyro;
import edu.wpi.first.wpilibj.motorcontrol.MotorControllerGroup;
import edu.wpi.first.wpilibj2.command.SubsystemBase;

public class DriveSubsystem extends SubsystemBase {
  // The motors on the left side of the drive.
  private final WPI_TalonFX m_leftMotor1 = new WPI_TalonFX(DriveConstants.kLeftMotor1Port);
  private final WPI_TalonFX m_leftMotor2 = new WPI_TalonFX(DriveConstants.kLeftMotor2Port);

  private final WPI_TalonFX m_rightMotor1 = new WPI_TalonFX(DriveConstants.kRightMotor1Port);
  private final WPI_TalonFX m_rightMotor2 = new WPI_TalonFX(DriveConstants.kRightMotor2Port);


  // The robot's drive
 

  // The gyro sensor
  private final AHRS m_gyro = new AHRS();

  // Odometry class for tracking robot pose
  private final DifferentialDriveOdometry m_odometry;

  /** Creates a new DriveSubsystem. */
  public DriveSubsystem() {
    // We need to invert one side of the drivetrain so that positive voltages
    // result in both sides moving forward. Depending on how your robot's
    // gearbox is constructed, you might have to invert the left side instead.
    //m_leftMotor1.setInverted(true);
    //m_leftMotor2.setInverted(true);

    // Sets the distance per pulse for the encoders

    resetEncoders();
    m_odometry = new DifferentialDriveOdometry(m_gyro.getRotation2d());
  }

  private double getLeftDistance() {
    return -m_leftMotor1.getSelectedSensorPosition() * DriveConstants.kEncoderDistancePerPulse;
  }

  private double getRightDistance() {
    return m_rightMotor1.getSelectedSensorPosition() * DriveConstants.kEncoderDistancePerPulse;
  }

  private double getLeftVelocity() {
    return -m_leftMotor1.getSelectedSensorVelocity() * DriveConstants.kEncoderDistancePerPulse * 10;
  }

  private double getRightVelocity() {
    return m_rightMotor1.getSelectedSensorVelocity() * DriveConstants.kEncoderDistancePerPulse * 10;
  }

  @Override
  public void periodic() {
    // Update the odometry in the periodic block
    m_odometry.update(
        m_gyro.getRotation2d(), getLeftDistance(), getRightDistance());
  }

  /**
   * Returns the currently-estimated pose of the robot.
   *
   * @return The pose.
   */
  public Pose2d getPose() {
    return m_odometry.getPoseMeters();
  }

  /**
   * Returns the current wheel speeds of the robot.
   *
   * @return The current wheel speeds.
   */
  public DifferentialDriveWheelSpeeds getWheelSpeeds() {
    return new DifferentialDriveWheelSpeeds(getLeftVelocity(), getRightVelocity());
  }

  /**
   * Resets the odometry to the specified pose.
   *
   * @param pose The pose to which to set the odometry.
   */
  public void resetOdometry(Pose2d pose) {
    resetEncoders();
    m_odometry.resetPosition(pose, m_gyro.getRotation2d());
  }

  public void driveArcade(double output, double turn){
    double leftOutput = output + turn, rightOutput = output - turn;
    tankDriveVolts(10*leftOutput, 10*rightOutput);
  }

  public void driveTank(double left, double right){
    m_leftMotor1.set(-left);
    m_leftMotor2.set(-left);
    m_rightMotor1.set(right);
    m_rightMotor2.set(right);
  }

  /**
   * Controls the left and right sides of the drive directly with voltages.
   *
   * @param leftVolts the commanded left output
   * @param rightVolts the commanded right output
   */
  public void tankDriveVolts(double leftVolts, double rightVolts) {
    m_leftMotor1.setVoltage(-leftVolts);
    m_leftMotor2.setVoltage(-leftVolts);
    m_rightMotor1.setVoltage(rightVolts);
    m_rightMotor2.setVoltage(rightVolts);
  }

  /** Resets the drive encoders to currently read a position of 0. */
  public void resetEncoders() {
    m_leftMotor1.setSelectedSensorPosition(0);
    m_rightMotor1.setSelectedSensorPosition(0);
  }

  /**
   * Gets the average distance of the two encoders.
   *
   * @return the average of the two encoder readings
   */
  public double getAverageEncoderDistance() {
    return (getLeftDistance() + getRightDistance()) / 2.0;
  }


  /** Zeroes the heading of the robot. */
  public void zeroHeading() {
    m_gyro.reset();
  }

  /**
   * Returns the heading of the robot.
   *
   * @return the robot's heading in degrees, assuming positive values are clockwise.
   */
  public double getHeading() {
    return m_gyro.getAngle();
  }

  /**
   * Returns the turn rate of the robot.
   *
   * @return The turn rate of the robot, in degrees per second, assuming positive values are clockwise.
   */
  public double getTurnRate() {
    return -m_gyro.getRawGyroZ();
  }
}
